

var compass = (function($) {

	function getDirection(origin, destination) {
		var bearing = calcBearing(origin, destination),
			compass = setCompassPoint(bearing);
		destination.compass = compass;
		return compass;
	}

	function calcBearing(origin, destination) { //find_bearing
		var dest_lng = calcRadius(destination.getPosition().lng() - origin.getPosition().lng()),
		y = Math.sin(dest_lng) * Math.cos(calcRadius(destination.getPosition().lat())),
		x = Math.cos(calcRadius(origin.getPosition().lat()))*Math.sin(calcRadius(destination.getPosition().lat())) - Math.sin(calcRadius(origin.getPosition().lat()))*Math.cos(calcRadius(destination.getPosition().lat()))*Math.cos(dest_lng),
		bearing = calcDegree(Math.atan2(y, x));

		return ((bearing + 360) % 360);
	}

	function calcRadius(data) { // rad
		return data * Math.PI / 180; 
	}

	function calcDegree(data){ // degree
		return data * 180 / Math.PI;
	}

	function setCompassPoint(bearing){
		var compass;
		bearing = ((bearing%360)+360)%360; // normalise to 0..360

		position = Math.round(bearing*8/360)%8;

		switch (position) {
			case 0: compass = 'N';  break;
			case 1: compass = 'NE'; break;
			case 2: compass = 'E';  break;
			case 3: compass = 'SE'; break;
			case 4: compass = 'S';  break;
			case 5: compass = 'SW'; break;
			case 6: compass = 'W';  break;
			case 7: compass = 'NW'; break;
		}

		return compass;
	}


	return {
		getDirection: getDirection
	};

})(jQuery);