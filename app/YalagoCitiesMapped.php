<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class YalagoCitiesMapped extends Model
{
    protected $fillable = [
        'yalago_city_id','eroam_city_id'
    ];
    protected $table = 'yalagocitiesmapped';
    protected $primaryKey = 'id';
    use SoftDeletes;
    public $timestamps = false; 

    protected $dates = ['deleted_at'];
    
    public function yalago_city()
    {
        return $this->hasOne('App\YalagoLocations', 'locationId', 'yalago_city_id');
    }
}
