<?php

namespace App;
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Eloquent;

class Country extends Eloquent {

    protected $table = 'zcountries';

    protected $guarded = array('id');

    public function region()
    {
        return $this->belongsTo('App\Region', 'region_id');
    }

    public function city(){
        return $this->hasMany('App\City');
    }

    public function cityEnabled(){
        return $this->hasMany('App\City')->where('is_disabled', 0);
    }

    public function scopeRegion($query)
    {
        return $query->select('zCountries.*', 'tblRegion.region_name')
            ->leftJoin('tblCountry', 'tblCountry.CountryCode', '=', 'zCountries.code')
            ->leftJoin('tblRegion', 'tblRegion.region_id', '=', 'tblCountry.region_id');
    }
    
    public static function geCountryList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10)
    {
        return Country::from('zcountries as c')
                    ->leftJoin('zregions as r','c.region_id','=','r.id')
                    ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where('c.'.$sSearchBy,'like','%'.$sSearchStr.'%');
                        })
                    ->select(
                        'c.id as id',
                        'c.name as name',
                        'c.code as code',
                        'c.country_code as country_code',
                        'c.show_on_eroam as show_on_eroam',
                        'c.created_at as created_at',
                        'c.updated_at as updated_at',
                        'r.name as region_name',
                        'r.id as region_id'
                        )
                    ->orderBy($sOrderField, $sOrderBy)
                    ->paginate($nShowRecord);
    }

    public static function getCountryByCode($code) {   
        $country = Country::select('name')->where('country_code',$code)->first();
        return $country;
    }

    // added by miguel
    // function is used in cms city page; search cities by country name;
    // start
    public function scope_search_by_name( $query, $name )
    {
        return $query->where('', 'like', '%'.$name.'%');
    }
    // end

}


