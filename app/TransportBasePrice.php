<?php
namespace App;
use Eloquent;

class TransportBasePrice extends Eloquent {

	protected $table = 'ztransportbaseprices';

    protected $guarded = array('id');


    protected $dates = ['deleted_at'];    

    public function tranpsort_price(){
    	return $this->hasOne('TransportPrice', 'id', 'transport_price_id');
    }

}
 
?>