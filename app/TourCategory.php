<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourCategory extends Model
{
    protected $fillable = [
        'category_id','tour_id'
    ];
    protected $table = 'tbltourcategory';
    public $timestamps = false;
}
