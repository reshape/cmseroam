<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryConfig extends Model
{
   	protected $table = 'inventory_config';
}
