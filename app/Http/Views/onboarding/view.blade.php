@extends( 'layout/mainlayout' )

@section('content')
	<div class="content-container">
		<div class="box-wrapper">
			<p class="h4">Licensee Information</p>
			<hr>
			<div class="row">
				<div class="col-sm-6">
					<p>Name: {{ $licensee['first_name'].' '.$licensee['last_name'] }}</p>
				</div>
				<div class="col-sm-6">
					<p>Business Name: {{ $licensee['business_name'] }}</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<p>Email: {{ $licensee['email'] }}</p>
				</div>
				<div class="col-sm-6">
					<p>Phone Number: {{ $licensee['phone_number'] }}</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<p>website: {{ $licensee['website'] }}</p>
				</div>
				<div class="col-sm-6">
					<p>Timezone: {{ $licensee->timezone->name }}</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<p>Address: {{ $licensee['apartment'].', '.$licensee['street'].' '.$licensee['city'].' '.$licensee['state'].' '.$licensee->country->name }}</p>
				</div>
			</div>
		</div>
		<div class="box-wrapper">
			<p class="h4">Domains</p>
			<hr>
			@foreach($domains as $domain)
				<p class="h5"> {{ $loop->iteration.'. '.$domain['name'] }} </p>

				@isset($domain['frontend'])
				<div class="panel panel-default">
					<div class="panel-heading">Frontend</div>
					<div class="panel-body">
						<img src="{{ url('uploads/onboardingLogo/'.$domain['frontend']['logo']) }}" width="100px">
						<table class="table table-responsive">
							<tr>
								<td>Template : {{ $domain['frontend']['template_id'] }}</td>
								<td>Webfont : {{ $domain['frontend']->webfont->name }}</td>
							</tr>
							<tr>
								<td>Theme Color : {{ $domain['frontend']['theme_color'] }}</td>
								<td>Font Color : {{ $domain['frontend']['font_color'] }}</td>
							</tr>
							<tr>
								<td>Header Color : {{ $domain['frontend']['header_color'] }}</td>
								<td>Footer Color : {{ $domain['frontend']['footer_color'] }}</td>
							</tr>
							<tr>
								<td>Search Color : {{ $domain['frontend']['search_color'] }}</td>
								<td>Consultant : @if($domain['frontend']['consultant'] === 1) {{ 'Yes' }} @else {{ 'No' }} @endif</td>
							</tr>
						</table>
					</div>
				</div>
				@endisset
				@if(!$domain['products']->isEmpty())
					<div class="panel panel-default">
						<div class="panel-heading">Products</div>
						<div class="panel-body">
							<div class="row">
							@foreach($domain['products'] as $product)
							<div class="col-sm-6">
								{{ $product['product']['name'] }}
							</div>
							@endforeach
							</div>
						</div>
					</div>
				@endif
				@if(!$domain['location']->isEmpty())
				<p><b>Location</b></p>
				@php $arr = []; 

				foreach($domain['location'] as $location) {
					$arr[$location['location_country']['name']][] = ['city_id'=>$location['location_city']['name']];
				}
				@endphp
				<div class="row">
				@foreach($arr as $key => $value)
					<div class="col-sm-6">
						<div class="panel panel-default">
							<div class="panel-heading">{{ $key }}</div>
							<div class="panel-body">
								<div class="row">
									@foreach($value as $val)
										<div class="col-sm-6">
										{{ $val['city_id'] }}
										</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				@endforeach
				</div>
				@endif	
				@if(!$domain['backend']->isEmpty())
				<div class="panel panel-default">
					<div class="panel-heading">Backend</div>
					<div class="panel-body">
						@php $arr = [];
						foreach($domain['backend'] as $backend) {
							$arr[$backend['config']['type']][] = ['item'=> $backend['config']['item']];
						}
						@endphp

						<div class="row">
						@foreach($arr as $key => $value)
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">{{ $key }}</div>
								<div class="panel-body">
									<div class="row">
										@foreach($value as $val)
											<div class="col-sm-6">
											{{ $loop->iteration.'. ' }}{!! $val['item'] !!}
											</div>
											@if($loop->iteration%2 === 0)
												</div>
												<div class="row">
											@endif
										@endforeach
									</div>
								</div>
							</div>
						</div>
						@if($loop->iteration%2 === 0)
							</div>
							<div class="row">
						@endif
						@endforeach
						</div>
					</div>
				</div>
				@endif

				@if(!$domain['inventory']->isEmpty())
					@php 
					$result = [];
					foreach ($domain['inventory'] as $inventory) {
						if(!empty($inventory['inventory_config']['parent'])) {
						    $result[$inventory['inventory_config']['source'].' '.$inventory['inventory_config']['type']][$inventory['inventory_config']['parent']][] = ['name' => $inventory['inventory_config']['name']];
						} else {
						    $result[$inventory['inventory_config']['source'].' '.$inventory['inventory_config']['type']][] = ['name' => $inventory['inventory_config']['name']];
						}
					}
					@endphp
					<div class="panel panel-default">
						<div class="panel-heading">Inventory</div>
						<div class="panel-body">
							<div class="row">
								@foreach($result as $key => $arr)
								<div class="col-sm-6">
									<div class="panel panel-default">
										<div class="panel-heading">{{ $key }}</div>
										<div class="panel-body">
											@foreach($arr as $key => $val)
				      	 						@if(array_key_exists('name',$val))
				      	 							<p>{{ $val['name'] }}</p>
				      	 						@else
			      	 								<div class="panel panel-default">
			      	 									<div class="panel-heading">{{ $key }}</div>
			      	 									<div class="panel-body">
				      	 									<div class="row">
				      	 										@foreach($val as $k => $v)
				      	 										<div class="col-sm-6">{{ $v['name'] }}</div>
			      	 											@endforeach		
				      	 									</div>
			      	 									</div>
			      	 								</div>
				      	 						@endif
				        					@endforeach
		        						</div>
		        					</div>
								</div>
								@if($loop->iteration%2 === 0)
									</div>
									<div class="row">
								@endif
								@endforeach
							</div>
						</div>
					</div>
				@endif
			@endforeach
		</div>
		<div class="box-wrapper">
			<p class="h4">Commission</p>
			<hr>
				@foreach($commissions as $commission)
				<div class="row">
					<div class="col-sm-12"><b>{{ ucfirst($commission['product_type']) }}</b></div>
					<div class="col-sm-6">Rate Type: {{ $commission['rate_type'] }}</div>	
					<div class="col-sm-6">Agent Enterprice: {{ $commission['agent_enterprice'] }}</div>	
					<div class="col-sm-6">Agent Wholesale: {{ $commission['agent_wholesale'] }}</div>	
					<div class="col-sm-6">Reseller Commission: {{ $commission['reseller_commission'] }}</div>	
				</div>
				<br>
				@endforeach
		</div>
		<div class="box-wrapper">
			<p class="h4">Account</p>
			<hr>
			<div class="row">
				<div class="col-sm-6">Mail Host: {{ $account['mail_host'] }}</div>
				<div class="col-sm-6">Mail Port: {{ $account['mail_port'] }}</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-6">Mail Username: {{ $account['mail_username'] }}</div>
				<div class="col-sm-6">Mail Password: {{ $account['mail_password'] }}</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-6">Mail from Address: {{ $account['mail_address'] }}</div>
				<div class="col-sm-6">Mail from name: {{ $account['mail_name'] }}</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-6">Mail Encryption: {{ $account['mail_encryption'] }}</div>
			</div>
			<br>
			<p></p>
			<div class="row">
				<div class="col-sm-12">Additional Notes: {{ $account['notes'] }}</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-6">Tax: {{ $account['tax'] }}</div>
				<div class="col-sm-6">Order ID Format: {{ $account['order_id_format'] }}</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-6">Checkout / Payment Confirmation Language: {{ $account['checkout_language'] }}</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-12">
					Refund Policy: {!! $account['refund_policy'] !!}
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-12">
					Privacy Policy: {!! $account['privacy_policy'] !!}
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-12">
					Terms of Service: {!! $account['terms_of_service'] !!}
				</div>
			</div>
		</div>
	</div>
@endsection