<div class="box-wrapper" id="domainbox{{$id}}">
	<a data-toggle="collapse" data-parent="#accordion" href="#domain{{$id}}">
		<p>Consumer Front-End</p>
	</a>
	<div class="collapse" id="domain{{$id}}">
	    <form method="post" class="add-form" id="form{{$id}}" action="{{route('frontend.add')}}">
	        <input type="hidden" name="licensee_id" id="licensee_id" value="{{ $licensee_id }}">
	        {{ csrf_field() }}
	        <div class="box-wrapper m-t-20">
	            <div class="col-sm-6">
	                <div class="form-group">
	                    <label class="label-control">Domain Name *</label>
	                    <input type="text" name="domain_name" id="domain_name{{$id}}" class="form-control" placeholder="Domain Name">
	                </div>
	            </div>
	            <div class="clearfix"></div>
	        
	            <label class="label-control">Products *</label>	 
	            <div class="row">       
		            @foreach($products as $product)
		            <div class="col-sm-6">
		                <div class="form-group">
		                    <label class="radio-checkbox label_check m-t-10 @if($product['status'] == 0) disabled @endif" for="products-{{ $product['id'] }}-{{$id}}">
		                        <input type="checkbox" class="checkbox" id="products-{{ $product['id'] }}-{{$id}}" value="{{ $product['id'] }}" name="products[]" @if($product['status'] == 0) disabled data-toggle="tooltip" data-placement="top" title="Not available in Pilot" @endif >{{ $product['name'] }} 
		                    </label>
		                </div>
		            </div>
		            @endforeach
	        	</div>
	        </div>
	        <div class="box-wrapper m-t-20">
		        <p>Front-End CSS Stylng</p>
		        <div class="row">
		            <div class="col-sm-12">
		                <div class="form-group">
		                    <label class="label-control">Template</label>
		                    <select class="form-control disabled" disabled name="template_id" id="template_id{{$id}}" data-toggle="tooltip" data-placement="top" title="Not available in Pilot">
		                        <option value="0">Select Template</option>
		                    </select>
		                </div>
		            </div>
		        </div><br>
	        
	            <label class="label-control">Logo *</label>
		        <div class="row image_upload_div">
		            <div class="col-sm-12">
		                <div class="file-upload1">
		                    <input type="hidden" id="logo{{$id}}" name="logo" value=""/>
		                    <input type="file" class="file-input fileupload" id="themeLogo{{$id}}" name="themeLogo"/>
		                </div>
		                <span class="input-filename"></span>
		                <span id="image-submit-load{{$id}}"></span>
		                <input type="hidden" name="image_name" id="image_name{{$id}}" class="image_name" value="@isset($frontend['logo']){{$frontend['logo']}}@endisset">
		            </div>
		            <div class="small-centered columns error_message_image error"></div>
		        </div><br>

		        <label class="label-control">Favicon *</label>
                <div class="row image_upload_div">
                    <div class="col-sm-12">
                        <div class="file-upload1">
                        	<input type="hidden" id="favicon{{$id}}" name="favicon" value=""/>
                            <input type="file" class="file-input faviconupload form-control" id="themeFavicon{{$id}}" name="themeFavicon"/>
                        </div>
                        <span class="input-filename"></span>
                        <span id="image-submit-load-favicon{{$id}}"></span>
                        <input type="hidden" name="favicon_name" id="favicon_name{{$id}}" class="favicon_name" value="@isset($frontend['favicon']){{$frontend['favicon']}}@endisset">
                                
                    </div>
                    <div class="small-centered columns error_message_image_favicon error"></div>
               	</div><br>
                    
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="label-control">Browser Page title *</label>
                            <input type="text" name="page_title" id="page_title" class="form-control" placeholder="Page Title" value="@isset($frontend){{ $frontend['domain']['page_title'] }}@endisset">
                        </div>
                    </div>
                </div><br>

		        <div class="row">
		            <div class="col-sm-12">
		                <div class="form-group">
		                    <label class="label-control">Select Websafe Font *</label>
		                    <select class="form-control" name="webfont_id" id="webfont_id">
		                        @foreach($webfonts as $wid => $name)
		                        <option value="{{ $wid }}">{{ $name }}</option>
		                        @endforeach
		                    </select>
		                </div>
		            </div>
		        </div><br>

		        <div class="row">
		            <div class="col-sm-6">
		                <div class="form-group">
		                    <label class="radio-checkbox label_check m-t-15 disabled" for="consultant"><input type="checkbox" id="consultant{{$id}}" name="consultant" value="1" disabled data-toggle="tooltip" data-placement="top" title="Not available in Pilot">Consultant Front-End</label>
		                </div>
		            </div>
		        </div>
	        </div>
	        <div class="box-wrapper m-t-20">
		        <p>Select Colour Scheme</p>
		        <div class="row">
		            <div class="col-sm-6">
		                <div class="form-group">
		                    <label class="label-control">Select Theme Color *</label>
		                    <input autocomplete="off" id="theme_color{{$id}}" name="theme_color" type="text"  class="form-control colorpic"/>
		                </div>
		            </div>
		            <div class="col-sm-6">
		                <div class="form-group">
		                    <label class="label-control">Select Font Color *</label>
		                    <input autocomplete="off" id="font_color{{$id}}" name="font_color" type="text" class="form-control colorpic"/>
		                </div>
		            </div>
		            <div class="col-sm-6">
		                <div class="form-group">
		                    <label class="label-control">Select Header Bg Color *</label>
		                    <input autocomplete="off" id="header_color{{$id}}" name="header_color" type="text" class="form-control colorpic"/>
		                </div>
		            </div>
		            <div class="col-sm-6">
		                <div class="form-group">
		                    <label class="label-control">Select Footer Bg Color *</label>
		                    <input autocomplete="off" id="footer_color{{$id}}" name="footer_color" type="text" class="form-control colorpic"/>
		                </div>
		            </div>
		            <div class="col-sm-6">
		                <div class="form-group">
		                    <label class="label-control">Select Search Filter Bg Color *</label>
		                    <input autocomplete="off" id="search_color{{$id}}" name="search_color" type="text" class="form-control colorpic"/>
		                </div>
		            </div>
		        </div>
		    </div>

		    <div class="box-wrapper m-t-20">
                <p>Social Media Settings</p>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">Facebook page link </label>
                            <input type="text" name="facebook_link" id="facebook_link" class="form-control" >
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                    	    <label class="label-control">Twitter page link </label>
                        	<input type="text" name="twitter_link" id="twitter_link" class="form-control" >
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">LinkedIn page link </label>
                            <input type="text" name="linkedin_link" id="linkedin_link" class="form-control" >
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">Instagram page link </label>
                            <input type="text" name="instagram_link" id="instagram_link" class="form-control" >
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">Pinterest page link </label>
                            <input type="text" name="pinterest_link" id="pinterest_link" class="form-control" >
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">Google Plus page link </label>
                            <input type="text" name="google_plus_link" id="google_plus_link" class="form-control" >
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">YouTube page link </label>
                            <input type="text" name="youtube_link" id="youtube_link" class="form-control" >
                        </div>
                    </div>
                </div>
            </div>
	        
	        <div class="loader countriesLoader" style="display: none;"><img src="{{asset('assets/images/ajax-loader.gif')}}"></div>
	        <div class="m-t-20 row">
	            <div class="col-sm-offset-2 col-sm-8">
	                <div class="row">
	                    <div class="col-sm-6">
	                        <button type="sumbit" class="btn btn-primary btn-block">Save</button>
	                    </div>
	                    <div class="col-sm-6">
	                        <button class="btn btn-primary btn-block domainRemove" data-id="{{$id}}" data-issaved="false">Remove</button>
	                    </div>
	                </div>
	            </div>
	        </div>
		</form>
	</div>
</div>
<script type="text/javascript">
	var id = "{{$id}}";
	$("#form"+id).validate({
        ignore: [],
        rules: {
            domain_name: {
                required: true,
				url:true
            },
            page_title: {
                required: true
            },
            favicon_name: {
                required: true
            },
            facebook_link: {
                url:true
            },
            twitter_link: {
                url:true
            },
            linkedin_link: {
                url:true
            },
            instagram_link: {
                url:true
            },
            pinterest_link: {
                url:true
            },
            google_plus_link: {
                url:true
            },
            youtube_link: {
                url:true
            },
            'products[]': {
                required: true
            },
            webfont_id:{
                required: true,
            },
            image_name: {
                required: true,
            },
            theme_color: {
                required: true,
            },
            font_color: {
                required: true
            },
            header_color: {
                required: true
            },
            footer_color: {
                required: true,
            },
            search_color: {
                required: true
            }
        },

        errorPlacement: function (label, element) {
            label.addClass('error_c');
            if($(element).hasClass('checkbox')){
                label.insertAfter($(element).parents('.row').children('.col-sm-6:last-child'));
            }else if($(element).hasClass('image_name')){
                label.insertAfter($(element).parent('.col-sm-12'));
            }else if($(element).hasClass('favicon_name')){
                label.insertAfter($(element).parent('.col-sm-12'));
            }else{
                label.insertAfter($(element).parent('.form-group'));
            }
        },
        submitHandler: function (form) {
            $("#form"+id).sumbit();
        }
    });

    $('#themeLogo'+id).fileuploader({
        changeInput:'<div class="fileuploader-input">' +
                        '<div class="fileuploader-input-inner">' +
                            '<h3 class="fileuploader-input-caption"><span class="span{{$id}}"><i class="icon-add-photos"></i> Drag Theme Logo here to upload.</span></h3>' +
                        '</div>' +
                    '</div>' + 
                    '<div class="fileupload-link"><span>Click here to upload logo from your computer</span> (Dimension 200 X 60. Supported file types, .jpeg, .jpg, .png).</div>',
        theme: 'dragdrop',
        upload: {
            url: siteUrl('onboarding/upload-logo'),
            data: {logo:$('#logo'+id).val()},
            type: 'POST',
            enctype: 'multipart/form-data',
            start: true,
            synchron: true,
            beforeSend: null,
            onSuccess: function(result, item) {
                var data = result;
                if(data.success == true){
                    $('.fileuploader-items-list').find('li').remove();
                    item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
                    setTimeout(function() {
                        item.html.find('.progress-bar2').fadeOut(400);
                    }, 400);
                    item.name = data.data[0].image_name;
                    var img_url = data.data[0].image_path+'/'+data.data[0].image_name;

                    var img_html = '<img src="'+img_url+'" class="img-responsive">';
                    $("#image-submit-load"+id).html('');
                    $(".fileuploader-input .fileuploader-input-inner .fileuploader-input-caption .span"+id).html('Your file is selected');
                    $("#image-submit-load"+id).css('display','block').html(img_html);
                    $("#image_name"+id).val(data.data[0].image_name);
                    // $("input[name='image_name']").valid();
                    $('.error_message_image').html('');
                }else if(data.success == false) {
                    $('.fileuploader-items-list').find('li').remove();
                    $('.error_message_image').html('<p>'+data.error.message+'</p>');
                    $(".fileuploader-input .fileuploader-input-inner .fileuploader-input-caption span").html('<h3 class="fileuploader-input-caption"><span><i class="icon-add-photos"></i> Drag Theme Logo here to upload.</span></h3>');
                }else{
                    $('.fileuploader-items-list').find('li').remove();
                }

                // if warnings
                if (data.hasWarnings) {
                    for (var warning in data.warnings) {
                        alert(data.warnings);
                    }
                    item.html.removeClass('upload-successful').addClass('upload-failed');
                    return this.onError ? this.onError(item) : null;
                }
                
                
            },
            onError: function(result, item,response) {
                $('.error_message_image').html('<p>The logo must be a file of type: jpeg, jpg, png.</p>')
            },
            onComplete: null,
        }
    });

    $('#themeFavicon'+id).fileuploader({
        changeInput:'<div class="fileuploader-input">' +
                        '<div class="fileuploader-input-inner">' +
                            '<h3 class="fileuploader-input-caption"><span class="span{{$id}}"><i class="icon-add-photos"></i> Drag Theme Favicon here to upload.</span></h3>' +
                        '</div>' +
                    '</div>' + 
                    '<div class="fileupload-link"><span>Click here to upload favicon from your computer</span> (Dimension 16 X 16. Supported file types, .png, .ico).</div>',
        theme: 'dragdrop',
        upload: {
            url: siteUrl('onboarding/upload-favicon'),
            data: {logo:$('#favicon'+id).val()},
            type: 'POST',
            enctype: 'multipart/form-data',
            start: true,
            synchron: true,
            beforeSend: null,
            onSuccess: function(result, item) {
                var data = result;
                if(data.success == true){
                    $('.fileuploader-items-list').find('li').remove();
                    item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
                    setTimeout(function() {
                        item.html.find('.progress-bar2').fadeOut(400);
                    }, 400);
                    item.name = data.data[0].image_name;
                    var img_url = data.data[0].image_path+'/'+data.data[0].image_name;

                    var img_html = '<img src="'+img_url+'" class="img-responsive">';
                    $("#image-submit-load-favicon"+id).html('');
                    $(".fileuploader-input .fileuploader-input-inner .fileuploader-input-caption .span"+id).html('Your file is selected');
                    $("#image-submit-load-favicon"+id).css('display','block').html(img_html);
                    $("#favicon_name"+id).val(data.data[0].image_name);
                    // $("input[name='favicon_name']").valid();
                    $('.error_message_image_favicon').html('');
                }else if(data.success == false) {
                    $('.fileuploader-items-list').find('li').remove();
                    $('.error_message_image_favicon').html('<p>'+data.error.message+'</p>');
                    $(".fileuploader-input .fileuploader-input-inner .fileuploader-input-caption span").html('<h3 class="fileuploader-input-caption"><span><i class="icon-add-photos"></i> Drag Theme Favicon here to upload.</span></h3>');
                }else{
                    $('.fileuploader-items-list').find('li').remove();
                }

                // if warnings
                if (data.hasWarnings) {
                    for (var warning in data.warnings) {
                        alert(data.warnings);
                    }
                    item.html.removeClass('upload-successful').addClass('upload-failed');
                    return this.onError ? this.onError(item) : null;
                }
                
                
            },
            onError: function(result, item,response) {
                $('.error_message_image_favicon').html('<p>The favicon must be a file of type: png, ico.</p>')
            },
            onComplete: null,
        }
    });
</script>