@extends( 'layout/mainlayout' )
@section('content')
<div class="content-container">
    <h1 class="page-title">{{ trans('messages.update',['name' => 'User Details']) }}</h1>
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif
    </div>
    <br>
    <div class="box-wrapper">
        <form method="post" action="{{route('usermanagement.update',[$licensee_id])}}" class="add-form" id="user-form">
            {{csrf_field()}}
            @method('PUT')
            <div class="alert" role="alert" id="error_msg"></div>
            <input type="hidden" value="{{$licensee_id}}" name="licensee_id" id="licensee_id">
            <input type="hidden" value="{{$user['id']}}" name="id" id="id">
            @php $name = explode(' ',$user['name']);
                $first_name = $name[0];
                $last_name = $name[1];
            @endphp
            <div class="box-wrapper">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">First Name</label>
                            <input type="text" name="fname" id="fname" class="form-control" placeholder="First Name" value="{{ $first_name }}"> 
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">Last Name</label>
                            <input type="text" name="lname" id="lname" class="form-control" placeholder="Last Name" value="{{ $last_name }}"> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">Email Address</label>
                            <input type="text" name="username" id="username" class="form-control" placeholder="Email Address" value="{{ $user['username'] }}"> 
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">Contact Number</label>
                            <input type="text" name="contact_no" id="contact_no" class="form-control" placeholder="Contact Number" value="{{ $user['contact_no'] }}"> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        @php $domain_ids = array_column($user['user_domains']->toArray(),'domain_id'); @endphp
                        <div class="form-group">
                            <label class="label-control">Assign Domain/s</label>
                            <select class="form-control domains" name="domain_id[{{$user['id']}}][]" id="domain_id" multiple="multiple" placeholder="Select Domain">
                                @foreach($domains as $domain_key => $domain)
                                    <option value="{{$domain['id']}}"
                                    @if(in_array($domain['id'],$domain_ids)){{'selected'}}@endif
                                    >{{$domain['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">Role</label>
                            <select class="form-control" name="type" id="type">
                                <option value="">Select Role</option>
                                <option value="licensee_administrator" @if($user['type'] === 'licensee_administrator') selected @endif>Licensee Administrator</option>
                                <option value="brand_administrator" @if($user['type'] === 'brand_administrator') selected @endif>Brand Administrator</option>
                                <option value="product_manager" @if($user['type'] === 'product_manager') selected @endif>Product Manager</option>
                                <option value="agent" @if($user['type'] === 'agent') selected @endif>Agent</option>
                                <option value="consultant" @if($user['type'] === 'consultant') selected @endif>Consultant</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-t-20 row">
                <div class="col-sm-offset-2 col-sm-4">
                    <button type="sumbit" name="" class="btn btn-primary btn-block">Update</button>
                </div>
                <div class="col-sm-4">
                    <a href="{{route('onboarding.usermanagement',$licensee_id)}}" class="btn btn-primary btn-block">Cancel</a>
                </div>
            </div>
        </form>
    </div>
</div>
@stop
@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/blockui.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/onboarding.js') }}"></script>
<script type="text/javascript">
    $(".domains").selectize();
      $("#user-form").validate({
        ignore: [],
        rules: {
            fname: {
                required: true
            },
            lname: {
                required: true
            },
            username: {
                required: true,
                email: true
            },
            contact_no: {
                required: true,
                number: true
            },
            type: {
                required: true,
            },
            'domain_id[]': {
                required: true,
            }
        },
        errorPlacement: function (label, element) {
            label.addClass('error_c');
            label.insertAfter($(element).parent('.form-group'));
        },
        submitHandler: function (form) {
            $('.add-form').sumbit();
        }
    });
</script>
@endpush