@extends( 'layout/mainlayout' )

@section('custom-css')
<style type="text/css">
.select-user-type {
	display: inline-block;
	border-radius: 4px;
	text-align: center;
	font-size: 0.9rem;
	background: #dcdcdc;
	padding: 10px 25px;
	color: #333;
	transition: all .2s;
}
.select-user-type:hover, .select-user-type.selected {
	background: #666666;
	color: #fff;
}
.select-user-type.selected {
	cursor: default;
}
.search-box {
	margin: 25px 0;
	position: relative;
}
.search-box i.fa {
	position: absolute;
	top: 10px;
	left: 7px;
}
#search-key {
	padding-left: 25px;
}
.fa-check {
	color: #1c812f;
}
.fa-times,
.fa-exclamation-circle {
	color: #bd1b1b;
}
.user-name a {
	color: #2b78b0;
	font-weight: bold;
}
.ajax-loader {
	font-size: 1.5rem;
	display: none;
}
</style>
@stop

@section('content')

<div class="content-container">
    <h1 class="page-title">{{ trans('messages.manage_list_title', ['name' => 'Route']) }}</h1>

    <div class="row">
        <div class="small-12 small-centered columns delete-box hidden"></div> 
    </div>
    @if(Session::has('message'))
    <div class="row">
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div> 
    </div>
    <br>
    @endif
    <div class="box-wrapper">
        <a href="{{ route('route.route-create') }}" class="plus-icon" title="Add">
            <i class="icon-plus"></i>
        </a>
        <div class="row m-t-20 search-wrapper">
            <div class="col-md-5 col-sm-5">
                <div class="form-group">
                    <select name="starting_point" id="starting-point" class="form-control m-t-10">
                        <option value="">Select Starting Location</option>
                        @foreach ($oCities as $city)
                        <option {{ Input::get('starting_point') == $city->id ? 'selected' : '' }} value="{{ $city->id }}">
                            {{ isset($city->name)? $city->name:'' }}, {{ isset($city->country->name) ? $city->country->name:'' }}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-5 col-sm-5">
                <div class="form-group">
                    <select name="destination" id="destination" class="form-control m-t-10">
                        <option value="">Select Destination</option>
                        @foreach ($oCities as $city)
                        <option {{ Input::get('destination') == $city->id ? 'selected' : '' }} value="{{ $city->id }}">
                            {{ isset($city->name)? $city->name:'' }}, {{ isset($city->country->name) ? $city->country->name:'' }}
                        </option>
                        @endforeach
                    </select>
                </div>	
            </div>
            <div class="col-md-2 col-sm-2">
                <button class="button success tiny btn-primary btn-md" onclick="callRouteListing(event,'table_record')"><i class="fa fa-filter"></i>Filter</button>
            </div>
        </div>
        <div class="row m-t-30 search-wrapper">
            <div class="col-md-7 col-sm-7 m-t-10">
                <div class="input-group input-group-box">
                    <input type="text" class="form-control" placeholder="Search Route" name="search_str" value="{{ $sSearchStr }}">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit" onclick="callRouteListing(event,'table_record')"><i class="icon-search-domain"></i></button>
                    </span>
                </div>
                <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
                <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
            </div>
            <div class="col-md-5 col-sm-5">
                <div class="dropdown">
                        <button class="btn btn-primary btn-block dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>ACTION</button>
<!--                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="javascript://" id="delete">{{ trans('messages.delete_action') }}</a></li>
                        </ul>-->
                </div>
            </div>
        </div>
        <div class="m-t-30">
            <label>{{ trans('messages.show_record') }}</label>
            <select class="select-entry" name="show_record" onchange="getMoreListing(siteUrl('route/route-list?page=1'),event,'table_record');">
                <option value="10" {{ ($nShowRecord == 10) ? 'selected="selected"' : '' }}>10</option>
                <option value="20" {{ ($nShowRecord == 20) ? 'selected="selected"' : '' }}>20</option>
                <option value="30" {{ ($nShowRecord == 30) ? 'selected="selected"' : '' }}>30</option>
                <option value="50" {{ ($nShowRecord == 50) ? 'selected="selected"' : '' }}>50</option>
            </select>
            <label>{{ trans('messages.entries') }}</label>
        </div>
        <div class="table-responsive m-t-20 table_record">
           
            @include('WebView::route._route_list_ajax')
      
        </div>
    </div>

</div>

@stop
@section('custom-js')
<script type="text/javascript">

function getRouteSort(element,sOrderField)
{
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
    }
    else
    {
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
    }
    getMoreListing(siteUrl('route/route-list'),event,'table_record');
}

    var cmp_tour = [] ; 
    $(document).on('click',".cmp_tour_check",function () { 
        if(this.checked) {
            cmp_tour.push($(this).val());
            $('#dropdownMenu1').prop('disabled', false);
        }else{
            var removeItem = $(this).val();
            cmp_tour = $.grep(cmp_tour, function(value) {
                            return value != removeItem;
                          });
        }
        if(cmp_tour.length > 1){
            $("#manage_views, #manage_dates").hide();
        }
        else
            $("#manage_views, #manage_dates").show();
        if(cmp_tour.length == 0){
            $('#dropdownMenu1').prop('disabled', true);
        }
        if($('.cmp_tour_check:checked').length == $('.cmp_tour_check').length){
            $('#checkbox-00').prop('checked',true);
        }else{
            $('#checkbox-00').prop('checked',false);
        }
});

$(document).on('click','.label_check',function(){
    setupLabel();
});

function selectAllRow(ele)
{
    
    var cmp_tour = [];
    if(ele.checked === true) {
        // Iterate each checkbox
        $('.cmp_tour_check').each(function() {
            this.checked = true;
            cmp_tour.push($(this).val());
        });
        
        $('#dropdownMenu1').prop('disabled', false);
    }else{
        $('.cmp_tour_check').each(function() {
            this.checked = false;
        });
        cmp_tour = [] ;
        $('#dropdownMenu1').prop('disabled', true);
        $('#dropdownMenu1').prop('disabled', true);
    }
    setupLabel();
}
</script>
@stop