<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getCountrySort(this,'c.name');">{{ trans('messages.name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'c.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'c.optional_city');">{{ trans('messages.optional_city_thead') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'c.optional_city')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'co.name');"> {{ trans('messages.country_name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'co.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th class="text-center">Active</th>
        </tr>
    </thead>
    <tbody class="city_list_ajax">
    @if(count($oCityList) > 0)
        @include('WebView::common._more_city_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oCityList->count() , 'total'=>$oCityList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oCityList->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $oCityList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                getPaginationListing(siteUrl('common/city-list?page='+pageNumber),event,'table_record');
//                if(pageNumber > 1)
//                    callCityListing(event,'city_list_ajax',pageNumber);
//                else
//                    callCityListing(event,'table_record',pageNumber);
                $('#checkbox-00').prop('checked',false);
                setupLabel();
            }
        });
    });
</script>