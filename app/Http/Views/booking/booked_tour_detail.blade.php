@extends( 'layout/mainlayout' )
@section('custom-css')
<style>

    .error{
        color:red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .test{
        text-decoration: none;
    }
    .error_message{
        background: #f2dede;
        border: solid 1px #ebccd1;
        color: #a94442;
        padding: 11px;
        text-align: center;
        cursor: pointer;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }
</style>
@stop
@section('content')

<div class="content-container">
    <h1 class="page-title">{{ trans('messages.tour_information') }}</h1> 
    <div class="box-wrapper">
        <p><strong>{{ trans('messages.tour_detail') }}</strong></p>
        <div class="row">
            <div class="col-sm-2">
                <p>{{ trans('messages.tour_title') }}</p>
                <p>{{ trans('messages.provider') }}</p>
                <p>{{ trans('messages.tour_code') }}</p>
            </div>
            <div class="col-sm-10">
                <p>{{$oBookTours->tour_title}}</p>
                <p>{{$oBookTours->provider_name}}</p>
                <p>{{$oBookTours->tour_code}}</p>
            </div>
        </div>
    </div> 

    <div class="box-wrapper">
        <p><strong>{{ trans('messages.da_date') }}</strong></p>
        <div class="row">
            <div class="col-sm-2">{{ trans('messages.departure') }}</div>
            <div class="col-sm-4"><input type="text" value="{{ date( 'j/m/Y', strtotime($oBookTours->departure_date))}}" name="departure_date" id="departure_date" disabled /></div>
            <div class="col-sm-2">{{ trans('messages.arrival') }}</div>
            <div class="col-sm-4"><input type="text" value="{{ date( 'j/m/Y', strtotime($oBookTours->return_date))}}" name="return_date" id="return_date" disabled /></div>
        </div>
    </div>  

    <div class="box-wrapper">
        <p><strong>{{ trans('messages.lead_traveller_detail') }}</strong></p> 
        <div class="row">
            
            <div class="col-sm-2"><p>{{ trans('messages.first_name') }}</p></div>
            <div class="col-sm-4"><p><input type="text" value="{{ $oBookTours->FName }}" name="FName" id="FName" disabled /></p></div>
            <div class="col-sm-2"><p>{{ trans('messages.surname') }}</p></div>
            <div class="col-sm-4"><p><input type="text" value="{{ $oBookTours->SurName }}" name="SurName" id="SurName" disabled /></p></div>
        </div>

        <div class="row">
            <div class="col-sm-2"><p>{{ trans('messages.email') }}</p></div>
            <div class="col-sm-4"><p><input type="text" value="{{ $oBookTours->email }}" name="email" id="email" disabled /></p></div>
            <div class="col-sm-2"><p>{{ trans('messages.contact') }}</p></div>
            <div class="col-sm-4"><p><input type="text" value="{{ $oBookTours->contact }}" name="contact" id="contact" disabled /></p></div>            
        </div>

        <!-- <div class="row">
            <div class="col-sm-2"><p>{{ trans('messages.dob') }}</p></div>
            <div class="col-sm-4"><p><input type="text" value="{{ date( 'j/m/Y', strtotime($oBookTours->DOB))}}" name="DOB" id="DOB" disabled /></p></div>
            <div class="col-sm-2"><p>{{ trans('messages.title') }}</p></div>
            <div class="col-sm-4"><p>{{ $oBookTours->Title }}</p></div>
        </div> -->

        <!-- <div class="row">
            <div class="col-sm-2"><p>{{ trans('messages.country') }}</p></div>
            <div class="col-sm-4"><p>{{ $oBookTours->address}}</p></div>
            <div class="col-sm-2"><p>{{ trans('messages.gender') }}</p></div>
            <div class="col-sm-4"><p>{{ucfirst($oBookTours->gender)}}</p></div>
        </div> -->
    </div>

    <div class="box-wrapper">
        <p><strong>{{ trans('messages.price_detail') }}</strong></p>

        <p><div class="row">
            <div class="col-sm-2">{{ trans('messages.price') }}</div>
            <div class="col-sm-4">
                {{ $oBookTours->booking_currency . ' ' . number_format(str_replace(',', '', $oBookTours->price), 2, '.', '') }}
            </div>
            <div class="col-sm-2">{{ trans('messages.traveller_type') }}</div>
            <div class="col-sm-4">{{ trans('messages.lead_traveller') }}</div>
        </div></p>

        <p><div class="row">
            <div class="col-sm-2">{{ trans('messages.total_booking_price') }}</div>
            <div class="col-sm-4">{{ number_format(str_replace(',', '', $oBookTours->grand_total),2) }}</div>
            <div class="col-sm-2">{{ trans('messages.credit_card_charges') }}</div>
            <div class="col-sm-4">{{ number_format(str_replace(',', '', $oBookTours->credit_card_fee),2) }}</div>
        </div></p>

        <p>
            <div class="row">
                <div class="col-sm-2">{{ trans('messages.addon_total') }}</div>
                <div class="col-sm-4">{{ number_format(str_replace(',', '', $oBookTours->addon_total_price),2) }}</div>
                <div class="col-sm-2">{{ trans('messages.grand_total') }}</div>
                <div class="col-sm-4">{{ number_format(str_replace(',', '', $oBookTours->grand_total),2) }}</div>
            </div>
        </p>
    </div> 

    <div class="box-wrapper">
        <p><strong>{{ trans('messages.general_info') }}</strong></p>
        <div class="row">
            <div class="col-sm-6">
                <p> @if($oBookTours->RequestTextXML != '')
                        {{ $oBookTours->RequestTextXML }}
                    @else
                        N/A
                    @endif   
                </p>
            </div>
        </div>
    </div> 

    <div class="box-wrapper">
        <p><strong>{{ trans('messages.booking_type') }}</strong></p>
        <div class="row">
            <div class="col-sm-6">
                <p>{{ $oBookTours->BookingType}}</p>
            </div>
        </div>
    </div>

    @if($oBookTours->payment_method == 1)
        <div class="box-wrapper">
            <p><strong>{{ trans('messages.eway_gateway_info') }}</strong></p>
            <p><div class="row">
                <div class="col-sm-2">{{ trans('messages.transaction_status') }}</div>
                <div class="col-sm-4">{{ $oBookTours->eway_result }}</div>
                <div class="col-sm-2">{{ trans('messages.transaction_number') }}</div>
                <div class="col-sm-4">{{ $oBookTours->eway_transaction_no }}</div>
            </div></p>
        </div> 
    @endif

    <div class="box-wrapper">
        <p><strong>{{ trans('messages.supplier_voucher_sent') }}</strong></p>
        <div class="row">
            <div class="col-sm-3"><p>{{ trans('messages.supplier_paid') }}</p></div>
            <div class="col-sm-9">
                <label class="radio-checkbox label_check" for="checkbox-SupplierPaid">
                    <input type="checkbox" name="supplier_paid" id="checkbox-SupplierPaid" value="1"{{ ($oBookTours->supplier_paid == 1) ? 'checked' : '' }} > {{ trans('messages.yes') }}
                </label>
            </div>
        </div>

        <div class="row m-t-20" id="manualBookingDiv"  {{ ($oBookTours->supplier_paid == 1) ? '' : 'style="display:none;"' }}>
            <div class="col-sm-3"><p>{{ trans('messages.manual_booking_id') }}</p></div>
            <div class="col-sm-9">
                <p><input type="text" name="manual_booking_id" id="manual_booking_id" value="{{ $oBookTours->manual_booking_id }}" placeholder="Manual Booking Id"/></p>
                <label for="manual_booking_id" id="ManualBookingIdError" generated="true" class="error" style="display:none;">Manual Booking Id is required.</label>
            </div>
        </div>

        <div class="row m-t-20">
            <div class="col-sm-3"><p>{{ trans('messages.customer_voucher_sent') }}</p></div>
            <div class="col-sm-9">
                <label class="radio-checkbox label_check" for="checkbox-CustomerVocherSent">
                    <input type="checkbox" name="voucher_sent" id="checkbox-CustomerVocherSent" value="1" {{ ($oBookTours->voucher_sent == 1) ? 'checked' : '' }} > {{ trans('messages.yes') }}
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8"></div>
            <div class="col-sm-2">
                <input type="hidden" name="request_id" id="request_id" value="{{ $oBookTours->request_id }}" >
                <input class="btn btn-primary btn-block" type="button" name="btnUpdate" value="Update" id="btnUpdate">
            </div>
            <div class="col-sm-2">
                <a href="{{URL::to('booking/booked-tour-list')}}" class="btn btn-primary btn-block">Cancel</a>
            </div>
        </div>
    </div> 

    <div class="box-wrapper">
        <p><strong>{{ trans('messages.update_status') }}</strong></p>
        <div class="row">
            <div class="col-sm-3"><p>{{ trans('messages.status') }}</p></div>
            <div class="col-sm-9">
                <select name="status" id="status">
                    <option value="">Choose Status</option>
                    @if($oBookTours->voucher_sent == 0)
                        <option value="0" {{ ($oBookTours->status == 0) ? 'selected' : '' }} >Pending</option>
                        <option value="1" {{ ($oBookTours->status == 1) ? 'selected' : '' }} >Confirmed</option>
                        <option value="2" {{ ($oBookTours->status == 2) ? 'selected' : '' }} >Cancelled</option>
                        <option value="3" {{ ($oBookTours->status == 3) ? 'selected' : '' }} >DeleteRequest</option>
                    @elseif($oBookTours->voucher_sent == 1)
                        <option value="4" {{ ($oBookTours->status == 4) ? 'selected' : '' }} >FileRequest</option>
                    @endif
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8"></div>
            <div class="col-sm-2">
                <input class="btn btn-primary btn-block" type="button" name="updateStatus" value="Update" id="updateStatus">
            </div>
            <div class="col-sm-2">
                <a href="{{URL::to('booking/booked-tour-list')}}" class="btn btn-primary btn-block">Cancel</a>
            </div>
        </div> 

    </div>

    @stop
    @section('custom-js')
    <script>
        $(document).ready(function () {
            $("#checkbox-SupplierPaid").click(function () {
                if ($(this).is(':checked')) {
                    $("#manualBookingDiv").show();
                } else {
                    $("#manualBookingDiv").hide();
                }

            });

            $("#btnUpdate").click(function () {
                var supplier_paid = 0;
                var is_ajax = 1;
                if ($("#checkbox-SupplierPaid").is(':checked')) {
                    supplier_paid = 1;
                    is_ajax = 0;
                }

                var manual_booking_id = $("#manual_booking_id").val();
                if (supplier_paid == 1) {
                    if (manual_booking_id == '') {
                        $("#ManualBookingIdError").show();
                    } else {
                        is_ajax = 1;
                        $("#ManualBookingIdError").hide();
                    }
                }

                var voucher_sent = 0;
                if ($("#checkbox-CustomerVocherSent").is(':checked')) {
                    voucher_sent = 1;
                }

                var request_id = $('#request_id').val();

                if (is_ajax == 1) {
                    $.ajax({
                        method: 'post',
                        url: "{{ url('booking/tour-update-status') }}",
                        data: {
                            _token: '{{ csrf_token() }}',
                            supplier_paid: supplier_paid,
                            voucher_sent: voucher_sent,
                            manual_booking_id: manual_booking_id,
                            request_id: request_id
                        },
                        success: function (response) {
                            if (response) {
                                location.reload();
                            }
                        }
                    });
                } else {
                    $("#ManualBookingIdError").show();
                }
            });

            $("#updateStatus").click(function () {
                var manual_booking_id = $("#manual_booking_id").val();
                $.ajax({
                    method: 'post',
                    url: "{{ url('booking/tour-update-status') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        status: $("#status").val(),
                        request_id: $('#request_id').val()
                    },
                    success: function (response) {
                        if (response) {
                            location.reload();
                        }
                    }
                });
            });

        });

        $('.update_booking_id').click(function () {
            var leg_detail_id = $(this).attr('leg-detail-id');
            var booking_id = $(this).attr('booking-id');
            $('#leg_detail_id').val(leg_detail_id);
            $('#booking_id').val(booking_id);
        });


        $('body').on('click', '.save_booking_id', function (event) {
            var leg_detail_id = $('#leg_detail_id').val();
            var booking_id = $('#booking_id').val();

            if (booking_id) {
                $.ajax({
                    method: 'post',
                    url: '{{ url('itinerary_bookings / update - booking - id') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        leg_detail_id: leg_detail_id,
                        booking_id: booking_id
                    },
                    success: function (response) {
                        if (response) {
                            location.reload();
                        }
                    }
                });
            } else {
                $('.error_message').show();
            }

        });


    </script>
    @stop
