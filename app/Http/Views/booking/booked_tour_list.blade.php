@extends( 'layout/mainlayout' )
@section('custom-css')
<style>
    .success_message{
        color:green !important;
        text-align: center;
    }
    .update-region-btn i {
        margin-left: .5rem;
        font-size: 1.2rem;
    }
    .switch {
        margin-bottom: 0;
    }
</style>
@stop
@section('content')

<div class="content-container">
    <h1 class="page-title">{{ trans('messages.booking_managment') }}</h1> 
    <div class="box-wrapper">
        <div class="row m-t-20 search-wrapper">
            <div class="col-md-7 col-sm-7">
                <div class="input-group input-group-box">
                    <input type="text" class="form-control" name="search_str" value="{{ $sSearchStr }}" placeholder="Seach Bookings">
                    <span class="input-group-btn">
                        <button class="btn btn-default" onclick="getMoreListing('{{route('booking.booked-tour-list')}}',event,'table_record')" type="button">
                            <i class="icon-search-domain"></i>
                        </button>
                    </span>
                </div>
            </div>
            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
            <div class="col-md-5 col-sm-5">
                <div class="dropdown">
                    <button class="btn btn-primary btn-block dropdown-toggle"  type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" disabled>ACTION</button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    </ul>
                </div>
            </div>
        </div>

        <div class="table-responsive m-t-20 table_record">
            @include('WebView::booking._booked_tour_list_ajax')
        </div>
    </div>
</div>



@stop

@section('custom-js')
<script>
function getTourSort(element,sOrderField)
{
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
    }
    else
    {
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
    }
    getMoreListing(siteUrl('booking/booked-tour-list?page=1'),event,'table_record');
}
var cmp_coupon_multiple = [] ;
$(document).on('click',".cmp_coupon_check",function () { 

    if($('.cmp_coupon_check:checked').length == $('.cmp_coupon_check').length){
        $('#checkbox-00').prop('checked',true);
    }else{
        $('#checkbox-00').prop('checked',false);
    }
});
$(document).on('click','.label_check',function(){
    setupLabel();
});   
function selectAllRow(ele)
{
    var cmp_tour = [];
    if(ele.checked === true) {
        // Iterate each checkbox
        $('.cmp_coupon_check').each(function() {
            this.checked = true;
            cmp_tour.push($(this).val());
        });
        
        $('#dropdownMenu1').prop('disabled', false);
    }else{
        $('.cmp_coupon_check').each(function() {
            this.checked = false;
        });
        cmp_tour = [] ;
        $('#dropdownMenu1').prop('disabled', true);
        $('#dropdownMenu1').prop('disabled', true);
    }
    setupLabel();
}
</script>
@stop