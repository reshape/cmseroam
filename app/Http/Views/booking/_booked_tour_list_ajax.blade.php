<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00"><input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;</label>
            </th>
            <th onclick="getTourSort(this,'request_id');">{{ trans('messages.booking_id') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'request_id')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getTourSort(this,'i.request_date');">{{ trans('messages.booked') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'i.request_date')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getTourSort(this,'departure_date');">{{ trans('messages.departure') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'departure_date')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getTourSort(this,'tour_title');" style="width:20%">{{ trans('messages.tour_title') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'tour_title')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getTourSort(this,'FName');">{{ trans('messages.customer') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'FName')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getTourSort(this,'grand_total');">{{ trans('messages.price') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'grand_total')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th>{{ trans('messages.view') }}</th>
        </tr>
    </thead>
    <tbody class="tour_list_ajax">
        @include('WebView::booking._more_booked_tour_list')
        
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oBookTours->count() , 'total'=>$oBookTours->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>
<script type="text/javascript">
$(function() {
    $('.pagination').pagination({
        pages: {{ $oBookTours->lastPage() }},
        itemsOnPage: 10,
        currentPage: {{ $oBookTours->currentPage() }},
        displayedPages:2,
        edges:1,
        onPageClick(pageNumber, event){
            getPaginationListing(siteUrl('booking/booked-tour-list?page='+pageNumber),event,'table_record');
//            if(pageNumber > 1)
//                getMoreListing(siteUrl('booking/booked-tour-list?page='+pageNumber),event,'tour_list_ajax');
//            else
//                getMoreListing(siteUrl('booking/booked-tour-list?page='+pageNumber),event,'table_record');
            $('#checkbox-00').prop('checked',false);
            setupLabel();
        }
    });
});
</script>