@extends( 'layout/mainlayout')
@section('custom-css')
<style>

    .error{
        color:red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .test{
        text-decoration: none;
    }
    .error_message{
        background: #f2dede;
        border: solid 1px #ebccd1;
        color: #a94442;
        padding: 11px;
        text-align: center;
        cursor: pointer;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }

    .nav>li>a:focus, .nav>li>a:hover {
        background-color: #eee;
    }

    .address{
        margin-left: 45px;
        font-family: sans-serif;
        font-size: 14px;
    }
</style>
@stop
@section('content')

<div class="content-container" >
    <h1 class="page-title">PAX Details</h1> 

    <div class="container col-sm-12">
        <ul class="nav nav-pills nav-tabs">
            <li class="active"><a href="{{ route('booking.tour-pax-detail',['nItenaryId'=>$request_id])}}">PAX Details</a></li>
            <li><a href="{{ route('booking.tour-product-detail',['nItenaryId'=>$request_id])}}">Product Details</a></li>
            <li><a href="{{ route('booking.tour-supplier-detail',['nItenaryId'=>$request_id])}}">Supplier Details</a></li>
        </ul>
    </div>
    </br>
    </br>
	</br>
    </br>
    <div class="box-wrapper">
        <p class="h4">{{ trans('messages.lead_pax_detail') }}</p>
        <hr>
        <h5><strong></strong></h5>
        </br>
        <div class="panel panel-default">
            <div class="panel-heading">PAX Details</div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <tbody><tr>
                            <td>Account ID : {{!empty($oleadPaxDetials['user_id'])? $oleadPaxDetials['user_id']:'N/A'}} </td>
                            <td>Title : {{!empty($oleadPaxDetials['Title'])? $oleadPaxDetials['Title']:'N/A'}}</td>
                        </tr>

                        <tr>
                            <td>Given Name : {{!empty($oleadPaxDetials['FName'])? $oleadPaxDetials['FName']:'N/A'}}
							</td>
                            <td>Family Name : {{!empty($oleadPaxDetials['SurName'])? $oleadPaxDetials['SurName']:'N/A'}}</td>
                        </tr>

                        <tr>
                            <td>Other Names : N/A</td>
                            <td>Gender : {{!empty($oleadPaxDetials['gender'])? $oleadPaxDetials['gender']:'N/A'}}</td>

                        </tr>
                        <tr>
                            <td>Year of Birth : {{!empty($oleadPaxDetials['DOB'])? $oleadPaxDetials['DOB']:'N/A'}}</td>
                            <td>Nationality : {{!empty($oleadPaxDetials['nationality'])? getCityName($oleadPaxDetials['nationality']):'N/A'}}</td>
                        </tr>

                    </tbody></table>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Contact Details</div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <tbody><tr>
                            <td>Phone : {{!empty($oleadPaxDetials['contact'])? $oleadPaxDetials['contact']:'N/A'}} </td>
                            <td>Email Address : {{!empty($oleadPaxDetials['email'])? $oleadPaxDetials['email']:'N/A'}}</td>
                        </tr>

                    </tbody></table>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Address</div>
                            <div class="panel-body">
                                 <table class="table table-responsive">
                                    <tbody>
                                        <tr>
                                            <td>
												Address (Line One): {{!empty($oleadPaxDetials['billing_street_addr'])? $oleadPaxDetials['billing_street_addr']:'N/A'}}
											</td>
                                            <td>Address (Line Two): {{!empty($oleadPaxDetials['billing_additional_addr'])? $oleadPaxDetials['billing_additional_addr']:'N/A'}}</td>
                                        </tr>
                                        <tr>
                                            <td>City: {{!empty($oleadPaxDetials['suburb'])? $oleadPaxDetials['suburb']:'N/A'}}</td>
                                            <td>State: {{!empty($oleadPaxDetials['state'])? $oleadPaxDetials['state']:'N/A'}}</td>
                                        </tr>
                                        <tr>
                                            <td>Country: {{!empty($oleadPaxDetials['country'])? getCityName($oleadPaxDetials['country']):'N/A'}}  </td>
											<td></td>
                                        </tr>

                                    </tbody>
									</table>
                            </div>
                        </div>
                    </div>
                </div>

                <table class="table table-responsive">
                    <tbody><tr>
                            <td>Contact Method: Eroam </td>
                            <td></td>
                        </tr>

                    </tbody></table>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">{{ trans('messages.additional_information') }}</div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <tbody>
                        <tr>
                            <td>Information : {{!empty($oleadPaxDetials['notes'])? $oleadPaxDetials['notes']:'N/A'}} </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>  
    </br>
	
    <div class="box-wrapper">
        <p class="h4">{{ trans('messages.additional_pax_detail') }}</p>
        <hr>
         <?php 
		 if(!$oAdditionalPaxDetials->isEmpty()){ ?>
		    <?php $i = 1; ?>
            <?php foreach ($oAdditionalPaxDetials as $additional) { ?>
                <div class="panel panel-default">
                    <div class="panel-heading">PAX Details : person {{$i}} </div>
                    <div class="panel-body">
                        <table class="table table-responsive">
                            <tbody>
                                <tr>
                                    <td>Account ID : {{!empty($additional['user_id'])? $additional['user_id']:'N/A'}}</td>
                                    <td>Title : {{!empty($additional['Title'])? $additional['Title']:'N/A'}}</td>
                                </tr>

                                <tr>
                                    <td>Given Name : {{!empty($additional['first_name'])? $additional['first_name']:'N/A'}} </td>
                                    <td>Family Name : {{!empty($additional['family_name'])? $additional['family_name']:'N/A'}}</td>
                                </tr>

                                <tr>
                                    <td>Other Names : N/A</td>
									<td>Gender :    N/A<td> 
									</td>
								</tr>
                                <tr>
                                    <td>Year of Birth : N/A</td>
                                    <td>Nationality : {{!empty($additional['nationality'])? getCityName($additional['nationality']):'N/A'}}</td>
                                </tr>
							</tbody></table>
                    </div>
                </div>
             <?php  $i++;}
        } else {
            ?>
            <h6><strong>No Additional Pax Detail</strong></h6>
		<?php } ?>   
    </div> 

    <div class="col-sm-offset-2 col-sm-8">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="row">
                    <a href="{{ route('booking.tour-product-detail',['nItenaryId'=>$request_id])}}" class="btn btn-primary btn-block">Next</a>
                </div>
            </div>
        </div>
    </div>
    @stop