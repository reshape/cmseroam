@extends( 'layout/mainlayout')
@section('custom-css')
<style>

    .error{
        color:red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .test{
        text-decoration: none;
    }
    .error_message{
        background: #f2dede;
        border: solid 1px #ebccd1;
        color: #a94442;
        padding: 11px;
        text-align: center;
        cursor: pointer;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }

    .nav>li>a:focus, .nav>li>a:hover {
        background-color: #eee;
    }
    @import url("https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css");
    .panel-default 
    {
        border-color: transparent;
    }
    .panel-default > .panel-heading
    {
        background-color: transparent;
        border-color: transparent;
        padding: 0px 0;
    }
    .panel-group .title
    {
        cursor: pointer;
    }
    .panel-group .title span
    {
        font-size: 16px;
        font-family: 'Open Sans', sans-serif;
        color: #464646;
        font-weight: bold;
        text-transform: uppercase;
    }
    .panel-heading .title:before {

        font-family: FontAwesome;
        content:"\f106";
        font-size: 25px;
        padding-right: 10px;
        line-height: 25px;
        float: right;
    }
    .panel-heading .title.collapsed:before {
        font-size: 25px;
        padding-right: 10px;
        line-height: 25px;
        float: right;
        content:"\f107";
    }
    .panel-body{
        padding: 5px !important;
    }
</style>
@stop
@section('content')

<div class="content-container" style="overflow:hidden;">
    <h1 class="page-title">{{ trans('messages.product_details') }}</h1> 

    @include('WebView::booking.review_booking_menu')
    <?php $pos = 1 ?>
    @foreach($cities_arr as $key=>$val)
    <div class="box-wrapper">
        <p class="h4">Products Details: {{ucfirst($val)}}</p>
        <hr>
		@foreach($supplier_details_all[$val] as $supplierkey=>$supplierval)
		<?php //echo "<pre>";print_r($supplierval); echo "</pre>";?>
        <?php
        

        if ($pos == 1) {
            $accordion_status = "";
            $accordion_icon_status = "in";
        } else {
            $accordion_status = "collapsed";
            $accordion_icon_status = "";
        }
        ?>
        <div class="box-wrapper">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne{{$supplierval['leg_type']}}{{$supplierval['leg_detail_id']}}">  
                        <div class="title {{$accordion_status}}  tab{{$pos}}" data-role="title" data-toggle="collapse" href="#collapseOne{{$supplierval['leg_type']}}{{$supplierval['leg_detail_id']}}" aria-expanded="false" aria-controls="collapseOne{{$supplierval['leg_type']}}{{$supplierval['leg_detail_id']}}">
                            <p class="h4"><strong>{{ucfirst($supplierkey)}}</strong><p>
                        </div>
                    </div>
                    <div id="collapseOne{{$supplierval['leg_type']}}{{$supplierval['leg_detail_id']}}" class="panel-collapse collapse {{$accordion_icon_status}}" role="tabpanel" aria-labelledby="headingOne{{$supplierval['leg_type']}}{{$supplierval['leg_detail_id']}}">
                        <div class="panel-body">
                            <div class="box-body">
                                @if($supplierkey == 'hotel')

                                <div class="panel-body">
                                    <table class="table table-responsive">
                                        <tbody>
											@if($supplierval['supplier_transport_name'] != '')
                                             <tr>
                                                <td>Supplier Name: {{!empty($supplierval['supplier_hotel_name'])? $supplierval['supplier_hotel_name']:'N/A'}}
                                                <td>Supplier Contact Person : {{!empty($supplierval['supplier_hotel_reservation_name'])? $supplierval['supplier_hotel_reservation_name']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Supplier Phone: {{!empty($supplierval['supplier_hotel_reservation_landline']) ? $supplierval['supplier_hotel_reservation_landline']:'N/A'}}</td>
                                                <td>Supplier Email Address    : {{!empty($supplierval['supplier_hotel_reservation_email']) ? $supplierval['supplier_hotel_reservation_email']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Supplier Payment Terms : N/A</td>
                                                <td>Supplier Additional Notes : {{!empty($supplierval['supplier_hotel_special_notes']) ? strip_tags(htmlspecialchars_decode($supplierval['supplier_hotel_special_notes'])):'N/A'}}</td>
                                            </tr>
											@else
											<tr>
												<td>No Supplier Available</td>
											</tr>
											@endif
                                        </tbody></table>
                                </div>
                                @elseif($supplierkey == 'activities')
                                <div class="panel-body">
                                    <table class="table table-responsive">
                                        <tbody>
										@if($supplierval['supplier_activity_name'] != '')
                                            <tr>
                                                <td>Supplier Name: {{!empty($supplierval['supplier_activity_name'])? $supplierval['supplier_activity_name']:'N/A'}}
                                                <td>Supplier Contact Person : {{!empty($supplierval['supplier_activity_reservation_contact_name'])? $supplierval['supplier_activity_reservation_contact_name']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Supplier Phone: {{!empty($supplierval['supplier_activity_reservation_contact_landline']) ? $supplierval['supplier_activity_reservation_contact_landline']:'N/A'}}</td>
                                                <td>Supplier Email Address    : {{!empty($supplierval['supplier_activity_reservation_contact_email']) ? $supplierval['supplier_activity_reservation_contact_email']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Supplier Payment Terms : N/A</td>
                                                <td>Supplier Additional Notes : {{!empty($supplierval['supplier_activity_special_notes']) ? strip_tags(htmlspecialchars_decode($supplierval['supplier_activity_special_notes'])):'N/A'}}</td>
                                            </tr>
                                            @else
											<tr>
												<td>No Supplier Available</td>
											</tr>
											@endif
                                        </tbody></table>
                                </div>
                                @elseif($supplierkey == 'transport')
                                <div class="panel-body">
                                    <table class="table table-responsive">
                                        <tbody>
											@if($supplierval['supplier_activity_name'] != '')
                                            <tr>
                                                <td>Supplier Name:  {{!empty($supplierval['supplier_transport_name'])? $supplierval['supplier_transport_name']:'N/A'}}
                                                <td>Supplier Contact Person : {{!empty($supplierval['supplier_transport_reservation_contact_name'])? $supplierval['supplier_transport_reservation_contact_name']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Supplier Phone: {{!empty($supplierval['supplier_transport_reservation_contact_landline']) ? $supplierval['supplier_transport_reservation_contact_landline']:'N/A'}}</td>
                                                <td>Supplier Email Address    : {{!empty($supplierval['supplier_transport_reservation_contact_email']) ? $supplierval['supplier_transport_reservation_contact_email']:'N/A'}}</td>
                                            </tr>
                                            <tr>
                                                <td>Supplier Payment Terms : N/A</td>
                                                <td>Supplier Additional Notes : {{!empty($supplierval['supplier_transport_special_notes']) ? strip_tags(htmlspecialchars_decode($supplierval['supplier_transport_special_notes'])):'N/A'}}</td>
                                            </tr>
											 @else
											<tr>
												<td>No Supplier Available</td>
											</tr>
											@endif
                                            
                                        </tbody></table>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endforeach
    </div> 
    <?php $pos++ ?>	
    @endforeach
	<div class="m-t-20 row">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="row">
				<div class="col-sm-6">
					<a href="{{ route('booking.booking-product-detail',['nItenaryId'=>$nBookId]) }}" class="btn btn-primary btn-block">Previous</a>
				</div>
				<div class="col-sm-6">
				   <a href="{{ route('booking.booking-itenary-detail',['nItenaryId'=>$nBookId]) }}" class="btn btn-primary btn-block">Next</a>
				</div>
			</div>
		</div>
    </div>
    @stop


