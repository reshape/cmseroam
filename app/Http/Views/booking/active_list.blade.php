@extends( 'layout/mainlayout' )
@section('custom-css')
<style>
    .success_message{
        color:green !important;
        text-align: center;
    }
    .update-region-btn i {
        margin-left: .5rem;
        font-size: 1.2rem;
    }
    .switch {
        margin-bottom: 0;
    }
</style>
@stop
@section('content')

@if($oItineraries)
<div class="content-container">
		<h1 class="page-title">Active Booking</h1> 
		
	<div class="box-wrapper">
        <div class="row m-t-20 search-wrapper">
            <div class="col-md-7 col-sm-7">
                <div class="input-group input-group-box">
                    <input type="text" class="form-control" name="search_str" value="{{ $sSearchStr }}" placeholder="Seach Bookings">
					<span class="input-group-btn">
					    <button class="btn btn-default" onclick="getMoreListing('{{route('booking.pending','pending')}}',event,'table_record');"
						 type="button"><i class="icon-search-domain"></i></button>
					</span>
                </div>
            </div>
			<div class="col-md-4 col-sm-4">
                <div class="input-group input-group-box">
                    <select name="type" id="type" class="form-control"
					   onChange="searchByType('{{route('booking.pending','active')}}',event,'table_record');"
					>
					<option value="all" <?php if($type == 'all'){ echo "selected";}else{ echo ""; }?>> All</option>
					<option value="personal"  <?php if($type == 'personal'){ echo "selected";}else{ echo ""; }?>>Personal</option>
					</select>
                </div>
            </div>
            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
           
            
            <!--div class="col-md-5 col-sm-5">
                <div class="dropdown">
                    <button class="btn btn-primary btn-block dropdown-toggle"  type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >ACTION</button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">                                                                
                    </ul>
                </div>
            </div-->
        </div>
	
        <div class="table-responsive m-t-20 table_record">
            @include('WebView::booking._booking_active_ajax')
        </div>
    </div>
</div>


@else
<div class="row full-width-row">
    <div class="small-12 column">
        {{ trans('messages.no_record_found') }}
    </div>
</div>

@endif
@stop

@section('custom-js')
<script>
	function getTourSort(element,sOrderField)
    {
        if($(element).find( "i" ).hasClass('fa-caret-down'))
        {
            $(element).find( "i" ).removeClass('fa-caret-down');
            $(element).find( "i" ).addClass('fa-caret-up');
            $("input[name='order_field']").val(sOrderField);
            $("input[name='order_by']").val('desc');
        }
        else
        {
            $(element).find( "i" ).removeClass('fa-caret-up');
            $(element).find( "i" ).addClass('fa-caret-down');
            $("input[name='order_field']").val(sOrderField);
            $("input[name='order_by']").val('asc');
        }
		getMoreListing(siteUrl('booking/active/?page=1'),event,'table_record');
		
		
        
    }
    $(document).on('click',".cmp_check",function () { 

        if($('.cmp_check:checked').length == $('.cmp_check').length){
            $('#checkbox-00').prop('checked',true);
        }else{
            $('#checkbox-00').prop('checked',false);
        }
    });
	
	$(document).on('click','.label_check',function(){
        setupLabel();
    });             
    $(document).on('click','.update-status-btn',function(event) {
        var box = $(this).parent().siblings('.update-status-box');
        var span = $(this).parent();
        var statusId = span.data('status-id');
        box.html('<div class="row"><div class="large-8 columns form-group">' +
                '<select class="status-select form-control m-t-10">' +
                '<option value="Pending">Pending</option>' +
                '<option value="Confirm Status">Confirm Status</option>' +
                '<option value="Cancel Status">Cancel Status</option>' +
                '</select></div></div>' +
                '<div class="row"><div class="large-8 columns region_cls"><a href="#" data-status-id="' + statusId + '" class="button success tiny btn-primary btn-sm m-r-10 save-update-status-btn">Save</a><a href="#" class="button success tiny btn-primary btn-sm m-r-10 cancel-update-status-btn">Cancel</a>' +
                '</div></div>'
                ).hide().fadeIn(200);
    });
    $('body').on('click', '.save-update-status-btn', function(event) {
    event.preventDefault();
    var order_id = $(this).data('status-id');
    var status_id = $(this).parent().parent().prev().find('select.status-select').val();
    var span = $(this).parent().parent().parent().prev();
    var cancelBtn = $(this).next('.cancel-update-status-btn');
        $.ajax({
            method: 'post',
            url: '{{ url('booking/update-status') }}',
            data: {
            _token: '{{ csrf_token() }}',
                    order_id: order_id,
                    status_id: status_id
            },
            success: function(response) {
            span.find('.status-name').html(response).effect('highlight', {color: '#91E3AB'}, 300);
            span.data('status-id', order_id)
                    cancelBtn.click();
            }
        });                                             
    });
    $('body').on('click', '.cancel-update-status-btn', function(event) {
        event.preventDefault();
        var box = $(this).parent().parent().parent('.update-status-box');
        var span = box.prev('span');
        box.html('');
        span.show();
    });
	
	function getShowType(){
		return $("#type").val();
	}
	
	function searchByType(sUrl,oEvent,sClass)
	{
		showLoader();
		var type = getShowType();
		var orderField = getOrderField();
		var orderBy = getSortingOrder();
		var showRecord = getShowRecord();
		var searchBy = $(".search_by").val();
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: "POST",
			url: sUrl,
			 data: {type: type, order_field: orderField, order_by: orderBy,show_record:showRecord,search_by:searchBy},
			success: function (data) {
				$('.'+sClass).html(data);
				$('.switch1-state1').bootstrapSwitch();
				hideLoader();
			},
			error: function (data) {
			}
		});
	}
	
	function getBookingPaginationListing(sUrl,oEvent,sClass)
{
    showLoader();
	var type = getShowType();
    //var searchStr = getSearchString();
    var orderField = getOrderField();
    var orderBy = getSortingOrder();
    var showRecord = getShowRecord();
    //var searchBy = $(".search_by").val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: sUrl,
        data: {order_field: orderField, order_by: orderBy,show_record:showRecord,type:type},
        success: function (data) {
            $('.'+sClass).html(data);
            $('.switch1-state1').bootstrapSwitch();
            hideLoader();
        },
        error: function (data) {
        }
    });
}
</script>
@stop