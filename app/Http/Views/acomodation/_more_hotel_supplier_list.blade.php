@foreach ($oHotelList as $aHotel)				
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aHotel->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aHotel->id;?>" value="<?php echo $aHotel->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="{{ route('acomodation.hotel-supplier-create',[ 'nIdHotelSupplier' => $aHotel->id ])}}">
                {{ $aHotel->name }}
            </a>
        </td>
        <td>{{ $aHotel->abbreviation }}</td>
        <td class="text-center">
            <div class="switch tiny switch_cls">
                <!-- <a href="javaScript:void(0);" class="button success tiny btn-primary btn-sm">Info</a> -->
                <a href="{{ route('acomodation.hotel-supplier-create',[ 'nIdSupplier' => $aHotel->id ])}}" class="button success tiny btn-primary btn-sm">{{ trans('messages.update_btn') }}</a>
                <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('acomodation.hotel-supplier-delete',['nIdHotel' => $aHotel->id]) }}','{{ trans('messages.delete_label')}}')">
            </div>
        </td>
    </tr> 
@endforeach