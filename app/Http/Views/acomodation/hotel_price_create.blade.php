@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    @if(isset($oHotelPrice))
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Accomodation Room Price']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Accomodation Room Price']) }}</h1>
    @endif
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif

        <div class="alert-box-container" style="display:none;">
            <div data-alert class="alert-box" tabindex="0" aria-live="assertive" role="alertdialog">
                <span class="alert-box-message">
                    The supplier you have chosen doesnt have a season for this hotel yet. You can add a season at the HOTELS > MANAGE SEASONS menu
                </span>
            </div>
        </div>
    </div>
    <br>
    <?php $sDisable = '';?>
        @if(isset($oHotelPrice))
        <?php $sDisable = 'readonly'; //print_r($oHotelPrice);exit;?>
       {{ Form::model($oHotelPrice, array('url' => route('acomodation.hotel-price-create') ,'method'=>'POST','enctype'=>'multipart/form-data','id' => 'create')) }}
       @else
       {{Form::open(array('url' => route('acomodation.hotel-price-create'),'method'=>'Post','enctype'=>'multipart/form-data', 'id' => 'create')) }}
       @endif
    <div class="box-wrapper">

        <p>Supplier Name</p>
        @if(!Session::has('from_hotel_session'))
        <div class="form-group m-t-30">
            <label class="label-control">Hotel <span class="required">*</span></label>
            {{ Form::select('hotel_id',$oHotel,Input::old('hotel_id'),['class'=>'form-control','id'=>'hotel' ])}}
        </div>
        @if ( $errors->first( 'hotel_id' ) )
        <small class="error">{{ $errors->first('hotel_id') }}</small>
        @endif
        @endif	  
        <div class="form-group m-t-30">
            <label class="label-control">Room Type <span class="required">*</span></label>
            {{ Form::select('hotel_room_type_id',$oHotelRoomType,Input::old('hotel_room_type_id'),['class'=>'form-control','id'=>'hotel_room_type_id'])}}
        </div>
        @if ( $errors->first( 'hotel_room_type_id' ) )
        <small class="error">{{ $errors->first('hotel_room_type_id') }}</small>
        @endif
        @if(!Session::has('from_hotel_session'))
            <div class="form-group m-t-30">
                <label class="label-control">Supplier <span class="required">*</span></label>
                {{ Form::select('hotel_supplier_id',$oSupplier,Input::old('hotel_supplier_id'),['class'=>'form-control','id'=>'supplier'])}}
            </div>
            @if ( $errors->first( 'hotel_supplier_id' ) )
            <small class="error">{{ $errors->first('hotel_supplier_id') }}</small>
            @endif
            <div class="form-group m-t-30">
                <label class="label-control">Season <span class="required">*</span></label>
                <select name="hotel_season_id" class='form-control' id="season">
                    <option disabled selected>Select Hotel & Supplier First</option>
                </select>
            </div>
            @if ( $errors->first( 'hotel_season_id' ) )
            <small class="error">{{ $errors->first('hotel_season_id') }}</small>
            @endif
        @else
        {{ Form::hidden('from_hotel', 'true',['id' => 'from_hotel'] ) }}
        @endif
        <div class="row m-t-30">
            <div class="form-group">
                <label class="label-control">Base Price</label>
                <div class="row">
                    <div class="col-sm-10">
                        <input type="number" class="form-control" step="0.01" min="0" id="base-price" name="base_price" placeholder="Enter Base Price" value = "{{ (isset($oHotelPrice) && isset($oHotelPrice->base_price_obj)) ? $oHotelPrice->base_price_obj->base_price : ''}}" />
                    </div>
                    <div class="col-sm-2 text-right">
                        <span data-tooltip aria-haspopup="true" class="has-tip form-control" style="vertical-align: sub;"
                              title="This is the list of markups available for this hotel room.">
                            <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                        </span>
                    </div>	
                </div>	
            </div>

            @if ( $errors->first( 'price' ) )
            <small class="error">{{ $errors->first('price') }}</small>
            @endif
        </div>
        @if(!Session::has('from_hotel_session'))	
        <div class="row m-t-30">
            <div class="form-group">
                <label class="label-control">Currency</label>
                <?php
                $attributes = 'form-control';
                ?>
                <div class="row">
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="currency" disabled/>
                    </div>
                    <div class="col-sm-2 text-right">
                        <span data-tooltip aria-haspopup="true" class="has-tip form-control" style="vertical-align: sub;"
                              title="This is the currency set for this Hotel. You can edit this at the Manage Hotels sub-menu of the Hotel menu.">
                            <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                        </span>
                    </div>	
                </div>	
            </div>
        </div>
        @endif	
        <div class="form-group m-t-30">
            <label class="label-control">Mark Up <span class="required">*</span></label>
            <select class="form-control" id="markup" name="hotel_markup_id">
                <option selected disabled value="" >Select Hotel First</option>
                <?php foreach ($oMarkup as $key => $markup): ?>
                <option value="{{$markup->id}}" data-percentage='{{ $markup->markup_percentage->percentage }}' {{ (isset($oHotelPrice) && isset($oHotelPrice->hotel_markup_id) && $oHotelPrice->hotel_markup_id == $markup->id) ? 'selected' : '' }}>{{ $markup->name.' - '.$markup->markup_percentage->percentage.'%' }}</option>	
                <?php endforeach ?>
            </select>
        </div>
        @if ( $errors->first( 'hotel_markup_id' ) )
        <small class="error">{{ $errors->first('hotel_markup_id') }}</small>
        @endif
        <div class="row m-t-30">	
            <div class="form-group">
                <label class="label-control">Price with Markup</label>
                <div class="row">
                    <div class="col-sm-10">
                        <input type="number" class="form-control" step="any" min="0" disabled id="markedup-price"/>
                    </div>
                    <div class="col-sm-2 text-right">
                        <span data-tooltip aria-haspopup="true" class="has-tip form-control" style="vertical-align: sub;"
                              title="This is the price with the applied markup. This is the amount the users will see on the website.">
                            <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                        </span>
                    </div>	
                </div>	
            </div>	
        </div>   
    </div>
    <div class="box-wrapper"> 
        <p>Package</p>  
        <div class="form-group m-t-30">

            <label class="label-control">Breakfast Included</label>
            <div>
                <label class="radio-checkbox label_radio" for="radio-03">
                    <input type="radio" id="radio-03" value="1" name="with_breakfast" {{ (isset($oHotelPrice) && isset($oHotelPrice->with_breakfast) && $oHotelPrice->with_breakfast == 1) ? 'checked' : ''}}> Yes
                </label> 
                <label class="radio-checkbox label_radio" for="radio-04">
                    <input type="radio" id="radio-04" value="0" name="with_breakfast" {{ (isset($oHotelPrice) && isset($oHotelPrice->with_breakfast) && $oHotelPrice->with_breakfast == 0) ? 'checked' : ''}}> No
                </label>
            </div>    
        </div>
        <div class="form-group m-t-30">
            <label class="label-control">Allotment <span class="required">*</span></label>
            {{Form::number('allotment',Input::old('allotment'),['id'=>'allotment','class'=>'form-control','step'=>'any','min'=>0,'placeholder'=>'Enter Allotment'])}}
        </div>
        @if ( $errors->first( 'allotment' ) )
        <small class="error">{{ $errors->first('allotment') }}</small>
        @endif
       

         <div class="row m-t-30">
            <div class="col-sm-6">
                 <div class="form-group">
                    <label class="label-control">Release <span class="required">*</span></label>
                    {{Form::number('release',Input::old('release'),['id'=>'release','class'=>'form-control','step'=>'any','min'=>0,'placeholder'=>'Enter Release'])}}
                </div>
                @if ( $errors->first( 'release' ) )
                <small class="error">{{ $errors->first('release') }}</small>
                @endif
            </div>  

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Maximum Pax <span class="required">*</span></label>
                    {{Form::number('max_pax',Input::old('max_pax'),['id'=>'max_pax','class'=>'form-control','step'=>'any','min'=>1,'placeholder'=>'Enter Maximum Pax'])}}
                </div>
                @if ( $errors->first( 'max_pax' ) )
                <small class="error">{{ $errors->first('max_pax') }}</small>
                @endif 
            </div>

        </div>
    </div>
    <div class="m-t-20 row col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-sm-6">
                {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
            </div>
            <div class="col-sm-6">
                <a href="{{route('acomodation.hotel-price-list')}}" class="btn btn-primary btn-block">Cancel</a>
            </div>
        </div>
    </div>
        {{ Form::hidden('id', $nId) }}
        {{ Form::hidden('from_flag', $nFromFlag) }}
        {{ Form::hidden('id_price', $nHotelPriceId) }}
    {{Form::close()}}
</div>
@stop

@section('custom-css')
<style>
    .error{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .error_message{
        color:red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .my-alert-box{
        border-style: solid;
        border-width: 1px;
        display: block;
        font-size: 0.8125rem;
        font-weight: normal;
        margin-bottom: 1.25rem;
        padding: 0.875rem 1.5rem 0.875rem 0.875rem;
        position: relative;
        transition: opacity 300ms ease-out;
        background-color: #008CBA;
        border-color: #0078a0;
        color: #FFFFFF;
    }
    .my-alert-box .close{
        right: 0.25rem;
        background: inherit;
        color: #333333;
        font-size: 1.375rem;
        line-height: .9;
        margin-top: -0.6875rem;
        opacity: 0.3;
        padding: 0 6px 4px;
        position: absolute;
        top: 50%;
    }
</style>
@stop

@section('custom-js')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
//on update time all selected for that
$(document).ready(function(){
    getSeasonInfo();
    getCurrencyInfo();
    setTimeout(function(){basePriceCal();},500);
});
$('#hotel').change(function(){
    	getCurrencyInfo();
});


function getCurrencyInfo()
{
    var hotelid = $('#hotel option:selected').val();
    if(hotelid != ''){
        $.ajax({
                url : siteUrl('acomodation/get-hotel-info'),
                data : {
                        hotel_id : hotelid
                },
                success: function( response ){
                    if(response){
                        var options = '';
                        var currency = response.currency.code;
                        $('#currency').val(currency);
                    }					
                }, 
                error: function( jqXHR, textStatus, errorThrown ){
                        console.log(jqXHR+'; '+textStatus+'; '+errorThrown);
                }
        });
    }
}

function getSeasonInfo()
{
    $('.alert-box-container').hide();
    $('#hotel, #supplier, #season').removeClass('with_error');
    var idHotel = $('#hotel option:selected').val();
    var idSupplier = $('#supplier option:selected').val();
    if(idHotel != '' && idSupplier != ''){
        $.ajax({
                url : siteUrl('acomodation/get-season-info'),
                data : {
                        hotel_id : idHotel,
                        supplier_id : idSupplier
                },
                success: function( response ){
                        console.log(response);
                        if(response.length)
                        {
                                var selected = '{{ (isset($oHotelPrice) && isset($oHotelPrice->hotel_season_id)) ? $oHotelPrice->hotel_season_id : ''}}';
                                var select ='';
                                var options = '';
                                var seasons = response;
                                $.each(seasons, function(key, value){
                                    if(value.id == selected)
                                        var select = "selected";
                                    options += '<option value="'+value.id+'"' + select +'>'+value.name+'</option>';
                                });
                        }
                        else
                        {
                                options = "<option value='0'>Not available</option>";
                                $('#hotel, #supplier, #season').addClass('with_error').blur();
                                $('.alert-box-container').show();
                                window.setTimeout(function(){
                                        $('.alert-box-container').fadeOut();
                                        $('#hotel, #supplier, #season').removeClass('with_error');
                                }, 5000);
                        }
                        $('#season').html(options);
                }, 	
                error: function( jqXHR, textStatus, errorThrown ){
                        console.log(jqXHR+'; '+textStatus+'; '+errorThrown);
                }
        });	
    }
}

$('#base-price, #markup').change( function(){
    basePriceCal();
});

function basePriceCal()
{
    var markup = $('#markup option:selected').val();
    if( markup )
    {
            var basePrice = $('#base-price').val();
            var from_hotel = $('#from_flag').val();
            if(from_hotel == null){
                    var percentage = $('#markup option:selected').data('percentage');	
            }
            else{
                    var percentage = '31.0';
            }
            console.log('percentage:'+percentage);
            var markedupPrice = ( parseFloat( basePrice ) / ( 100 - parseFloat( percentage )) * 100 );
            console.log('markedupprice:'+roundToTwo( markedupPrice ));
            $('#markedup-price').val( roundToTwo( markedupPrice ) );
    }
}

$('#supplier').change(function(){
    getSeasonInfo();
});

$(function() {
    $( "#create" ).validate({
        rules: {
            hotel_id: 'required',
            hotel_room_type_id: 'required',
            hotel_supplier_id: 'required',
            hotel_season_id: 'required',
            base_price: 'required',
            hotel_markup_id: 'required',
            allotment: 'required',
            release: 'required',
            max_pax: 'required',
        },
        errorPlacement: function(error, element) {
            var placement = $(element).parent();
            if (placement) {
              $(error).insertAfter(placement)
            } else {
              error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
                form.submit();
            }
    });
});
</script>
@stop