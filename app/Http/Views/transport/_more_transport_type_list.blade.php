@foreach ($oTransportTypeList as $aType)				
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aType->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aType->id;?>" value="<?php echo $aType->id;?>">&nbsp;
            </label>
        </td>
        <td>{{ $aType->sequence + 1 }}</td>
        <td>{{ $aType->name}}</td>
        <td class="text-center">
            <div class="switch tiny switch_cls">
                <input class="show-on-eroam-btn switch1-state1" data-id="{{ $aType->id }}" id="show-on-eroam-{{ $aType->id }}" type="checkbox" {{ $aType->is_enabled == 1 ? 'checked' : '' }}>
                <label for="show-on-eroam-{{ $aType->id }}"></label>
            </div>
        </td>
    </tr> 
@endforeach