<?php //echo '<pre>'; print_r($aTransportSeason); exit(); ?>				
    <?php 
                            $i=1;
                            if(count($oTransportSeasonList) > 0){
								//echo "<pre>";print_r($oTransportSeasonList);die;
                             foreach ($oTransportSeasonList as $key => $transport): ?>
                          <tr class="clickable <?php echo (($key == 0) ? 'open':'')?>" data-toggle="collapse" id="row-{{$transport->id}}" data-target=".row-{{$transport->id}}">
                              <td><i class="icon-unfold-less"></i></td>
                              <td>{{$transport->from_city->name}}</td>
                              <td>{{$transport->to_city->name}}</td>
                              <td>{{$transport->transporttype->name}}</td>
                              <td>{{ !empty($transport->operator->name) ? $transport->operator->name: ''}}</td>
                              <td>-</td>  
                              <td>-</td>
                              <td>-</td>
                              <td>-</td>  
                              <td>-</td>
                              <td>-</td>
                              <td>-</td>
							  {{-- <td> --}}
								@php
									// $domain_array = [];
									// if($transport->domain_id){
									// 	$domain_array = explode(',',$transport->domain_id);
									// 	foreach($domain_array as $key1=>$value1){
									// 		echo domianName($value1)->name.'<br>';
									// 	}
									// }
								@endphp
							  {{-- </td> --}}
                              <td class="text-center">-</td>
                          </tr>
                              <?php 
                              foreach ($transport->price as $season): 
                                    if($i == 1){
                                        ?>
                                         <tr class="collapse row-{{$transport->id}} in" aria-expanded="true">   
                                        <?php
                                    }else{
                                        ?>
                                        <tr class="collapse row-{{$transport->id}}">
                                        <?php
                                    }
                                ?>
                                                          
                                  <td>
                                    <label class="radio-checkbox label_check" for="checkbox-{{$season->id}}">
                                        <input type="checkbox" id="checkbox-{{$season->id}}" value="{{$season->id}}">&nbsp;
                                    </label>
                                  </td>
                                  <td>-</td>  
                                  <td>-</td>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>{{$season->name}}</td>
                                    <td>{{date('d/m/y',strtotime($season->from))}}</td>
                                    <td>{{date('d/m/y',strtotime($season->to))}}</td>
                                    <td>{{$season->minimum_pax}}</td>
                                    <td style="{{ ($season->allotment < 10) ? 'color:red;font-weight:bold;' : '' }}">
                                        {{$season->allotment}}
                                    </td>
                                    <td>
                                        {{ ($season->currency) ? $season->currency->code : '' }}
                                        {{number_format(round($season->price, 2), 2, '.', ',')}}
                                    </td>
                                    <td>{{ !empty($season->supplier->name)? $season->supplier->name:''}}</td>
                                    <td class="text-center">
                                        <div class="switch tiny switch_cls">
                                            <a href="{{ route('transport.transport-season-create',[ 'id' => $season->id ])}}" class="button success tiny btn-primary btn-sm">{{ trans('messages.update_btn') }}</a>
                                            <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('transport.transport-season-delete',['id'=> $season->id]) }}','{{ trans('messages.delete_label')}}')">
                                        </div>
                                    </td>
                              </tr>
                            <?php endforeach;?>
                            <?php $i=0;?>
                        <?php endforeach;?>
                        <?php }else{
                  ?>
                   <tr><td colspan="15" class="text-center">I don't have any records!</td></tr>
                  <?php

                }
                ?>