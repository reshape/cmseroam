@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    <h1 class="page-title">{{ trans('messages.add_new_supplier') }}</h1>
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif
        @if ($errors->any())
        <div class="small-6 small-centered columns error-box">{{$errors->first()}}</div>
        @endif
    </div>
    <br>
    <div class="box-wrapper">

        
        @if($nId != '')
        <p>Update Airline</p>
        {{ Form::model($oTransportAirline, array('url' => route('transport.airline-create') ,'method'=>'POST','enctype'=>'multipart/form-data','id'=>'addTourForm')) }}
        @else
        <p>{{ trans('messages.add_new_airline') }}</p>
        {{ Form::open(array('url' => route('transport.airline-create'),'method'=>'Post','enctype'=>'multipart/form-data','id'=>'addTourForm')) }}
        @endif
        <div class="form-group m-t-30">
            <label class="label-control" for="airline_code">{{ trans('messages.airline_code') }}<span class="required">*</span></label>
            {{Form::text('airline_code',Input::old('airline_code'),['id'=>'airline_code','class'=>'form-control','placeholder'=>'Enter Airline Code'])}}

        </div>
        @if ( $errors->first( 'airline-code' ) )
        <small class="error">{{ $errors->first('airline-code') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control" for="airline_name">{{ trans('messages.airline_name') }}<span class="required">*</span></label>
            {{Form::text('airline_name',Input::old('airline_name'),['id'=>'airline_name','class'=>'form-control','placeholder'=>'Enter Airline Name'])}}
        </div>
        @if ( $errors->first( 'airline_name' ) )
        <small class="error">{{ $errors->first('airline_name') }}</small>
        @endif
        
        <input type="hidden" value="{{ $nId }}" name="id" />
        
        <div class="form-group m-t-30">
            <label for="country" class="right inline">Country</label>
            <select name="country_id" id="country" class="form-control m-t-10" required>
                <option value="" selected disabled>Select Country</option>
                @foreach ($countries as $country)
                <option value="{{ $country->id }}">{{ $country->name }} ({{ $country->code }})</option>
                @endforeach
            </select>
        </div>
    </div>   
    <div class="m-t-20 row col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-sm-6">
                {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
            </div>
            <div class="col-sm-6">
                <a href="{{ route('transport.transport-supplier-list')}}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
            </div>
        </div>
    </div>	
    {{Form::close()}}
</div>
@stop

@section('custom-css')
<style>
    .error{
        color:red !important;
    }
    .error_message{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    div .with_error{
        border:1px solid black;
    }
</style>
@stop

@section('custom-js')
<script>

    $(function () {

        tinymce.init({
            selector: '#description',
            height: 200,
            menubar: false
        });
        tinymce.init({
            selector: '#remarks',
            height: 200,
            menubar: false
        });
        tinymce.init({
            selector: '#special_notes',
            height: 200,
            menubar: false
        });
    });
</script>
@stop
