<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getTransportSort(this,'name');">{{ trans('messages.name') }} 
                <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'name' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getTransportSort(this,'marketing_contact_name');">{{ trans('messages.contact_name') }} 
                <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'marketing_contact_name' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getTransportSort(this,'marketing_contact_phone');">{{ trans('messages.contact_number') }} 
                <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'marketing_contact_phone' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getTransportSort(this,'marketing_contact_email');">{{ trans('messages.contact_email') }} 
                <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'marketing_contact_email' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th class="text-center">{{ trans('messages.thead_action') }}</th>
        </tr>
    </thead>
    <tbody class="city_list_ajax">
        @if(count($oTransportList) > 0)
        @include('WebView::transport._more_transport_operator_list')
        @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
        @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oTransportList->count() , 'total'=>$oTransportList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
        <ul class="pagination">
            
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oTransportList->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $oTransportList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                getPaginationListing(siteUrl('transport/transport-operator-list?page='+pageNumber),event,'table_record');
//                if(pageNumber > 1)
//                getMoreListing(siteUrl('transport/transport-operator-list?page='+pageNumber),event,'city_list_ajax');
//                else
//                getMoreListing(siteUrl('transport/transport-operator-list?page='+pageNumber),event,'table_record');
                $('#checkbox-00').prop('checked',false);
            }
        });
    });
</script>