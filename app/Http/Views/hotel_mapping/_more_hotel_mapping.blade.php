<?php 
                                    
    $city_id    = '';
    $country_id = $country;
    $hotelIDs   = '';
    foreach ($hotel_ids as $hotelID) {
        $hotelIDs = $hotelID->EANHotelID.",".$hotelIDs;
    }
    $hotelIDs   = explode(",",$hotelIDs);
    //echo "<pre>";print_r($hotelIDs);
    ?>
<table class="table">
    <thead>
        <tr>
        	<th>
                <label class="radio-checkbox label_check" for="checkbox-00">
            	   <input type="checkbox" id="checkbox-00" class="select_all_chk" value="1">&nbsp;
                </label>
            </th>
            <th onclick="getSortData(this, 'Name');">Name
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'Name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
			<th class="text-center">Address</th>
            <th onclick="getSortData(this, 'City');">City
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'City')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getSortData(this, 'StarRating');">Property Rating
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'StarRating')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getSortData(this, 'HighRate');">High Rate
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'HighRate')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getSortData(this, 'LowRate');">Low
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'LowRate')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
		</tr>
    </thead>
    <tbody>
        @if(!empty($propertyList))
            @foreach($propertyList as $property)
                <?php  $checked =  ""; ?>
                @if(in_array($property->EANHotelID,$hotelIDs))                        
                    <?php  $checked =  "checked"; ?>
                @endif
                <tr>
                    <td><label class="radio-checkbox label_check chkbox" for="checkbox-{{$property->EANHotelID}}">
                            <input type="checkbox" id="checkbox-{{$property->EANHotelID}}" value="{{$property->EANHotelID}}" data-hotelId="{{$property->EANHotelID}}" <?php echo $checked; ?> class="chkboxes">&nbsp;
                        </label>
                        <input type="hidden" id="page_no" value="{{ $propertyList->currentPage() }}" />
                    </td>
                    <td>{{$property->Name}}</td>
                    <td>{{$property->Address1}}</td>
                    <td>{{$property->City}}</td>
                    <td>{{$property->StarRating}}</td>
                    <td>{{ number_format($property->HighRate,2) }}</td>
                    <td>{{ number_format($property->LowRate,2) }}</td>
                </tr>
            @endforeach
        @endif
        <tr>
        </tr>				
	</tbody>
</table>
<div class="clearfix">
    @if(isset($propertyList) && count($propertyList) > 0)
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $propertyList->count() , 'total'=>$propertyList->total() ]) }}</p></div>
    @endif
</div>
<script type="text/javascript">
    $('.select_all_chk').click(function(){
        console.log('hii');
        if (this.checked) 
        {
            $(".chkbox").find('input[type=checkbox]').each(function () {
                    this.checked = true;                        
            });
        }
        else
        {
            $(".chkbox").find('input[type=checkbox]').each(function () {
                    this.checked = false;                       
            });
        }
        //setupLabel();
    });

    $(document).ready(function(){
        setupLabel();
    })
    $(document).on('click','.chkboxes',function(){
        if($('.chkboxes:checked').length == $('.chkboxes').length){
            $('#checkbox-00').prop('checked',true);
        }else{
            $('#checkbox-00').prop('checked',false);
        }
        setupLabel();
    });
</script>