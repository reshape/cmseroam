<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\CityImage;
use App\Hotel;
use App\HotelImage;
use DB;

class ImageScriptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $allCity= City::all();
      $allCityImg= CityImage::all();
      foreach($allCity as $cityData)
      {
        #update database   
        foreach($allCityImg as $allCityImgData)
           {
              if(($cityData->id)==($allCityImgData->city_id))
              {
                $cid=$cityData->id;
                $image=basename($allCityImgData->original);
                \DB::table('zcities')->where('id','=',$cityData->id)->update(['small_image'=>basename($allCityImgData->original)]);                              

              }

           }
           #copy image from one to another folders
           if($cityData->small_image!="")
           {
               $cid=$cityData->id;
               $image=$cityData->small_image;  
               $file2=$_SERVER['DOCUMENT_ROOT'].'/uploads/cities_old/'.$cid.'/'.$image.'';
                if(file_exists($file2))
                {
                   $file3=$_SERVER['DOCUMENT_ROOT'].'/uploads\cities';
                   if (!copy($file2, $file3.'/'.$image)) 
                   {
                   echo "failed to copy $file2...\n";
                   }else{
                   echo "copied $file2 into $file3\n";}
               }
   
           }

      }

      echo 'updated successfully';

    }



    public function imageScriptHotel()
    {
      //echo 'jai';exit;  
      $allHotel= Hotel::all();
      $allHotelImg= HotelImage::all();

      foreach($allHotel as $hotelData)
      {
        #update database   
        foreach($allHotelImg as $allHotelImgData)
           {
              if(($hotelData->id)==($allHotelImgData->hotel_id))
              {
                
                 $image=basename($allHotelImgData->original);
                \DB::table('zhotelimages')->where('hotel_id','=',$hotelData->id)->update(['original'=>$image]);  
                
                if($allHotelImgData->original!="")
                {
                    $hotelId=$allHotelImgData->hotel_id;
                    $image=$allHotelImgData->original;  
                    $file2=$_SERVER['DOCUMENT_ROOT'].'/uploads/hotels_old/'.$hotelId.'/'.$image.'';
                     if(file_exists($file2))
                     {
                        $file3=$_SERVER['DOCUMENT_ROOT'].'/uploads\hotels';
                        if (!copy($file2, $file3.'/'.$image)) 
                        {
                        echo "failed to copy $file2...\n";
                        }else{
                        echo "copied $file2 into $file3\n";}
                    }
        
                }

              }

           }
           #copy image from one to another folders

          

      }

      echo 'Hotel updated successfully';

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
