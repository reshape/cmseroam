<?php

namespace App\Http\Controllers;

use App\SpecialOffer;
use Illuminate\Http\Request;
use App\Hotel;
use App\Activity;
use App\Tour;
use App\Transport;
use App\User;
use App\ActivityBasePrice;
use App\TransportPrice;
use Validator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use DB;
use Session;

class SpecialOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function __construct()
    {
        $this->middleware('auth');
        session(['page_name' => 'specialoffer']);
    }

    public function index() {

    }
    public function OfferList(Request $oRequest)
    {
        $aData = session('special_offer') ? session('special_offer') : array();
        $oRequest->session()->forget('special_offer');
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) && $sSearchStr != $aData['search_str']) {
            $nPage = 1;
        }
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        $specialOffer = SpecialOffer::getList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);

        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$specialOffer->currentPage(),'special_offer');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::special_offer._special_offer_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::special_offer.index' : 'WebView::special_offer._special_offer_list';

        return \View::make($oViewName,compact('specialOffer','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $product_name = [];
        $special_offer = [];
        if($request->id) {
            $special_offer = SpecialOffer::where('id',$request->id)->first();
            if($special_offer->inventory_type === 'Accommodation') {
                $product_name = SpecialOffer::getAccommodations();
            }
            if($special_offer->inventory_type === 'Activity') {
                $product_name = SpecialOffer::getActivities();
            }
            if($special_offer->inventory_type === 'Tour') {
                $product_name = SpecialOffer::getTours();
            }
            if($special_offer->inventory_type === 'Transport') {
                $product_name = SpecialOffer::getTransports();
            }
        }

        $licensee = User::select('name')->where('type','licensee')->get();
        return \View::make('WebView::special_offer.create',compact('licensee','special_offer','product_name'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,SpecialOffer $special_offer)
    {
        $rules = [
            'offer_name' => 'required',
            'offer_type' => 'required',
            'inventory_type' => 'required',
            'product_name' => 'required',
            'client' => 'required',
            'selling_start_date' => 'required',
            'selling_end_date' => 'required',
            'travel_start_date' => 'required',
            'travel_end_date' => 'required'
        ];  

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return ['result' => 0,'errors' =>$validator->messages()];
        }

        $special_offer->offer_name = $request->offer_name;
        $special_offer->offer_type = $request->offer_type;
        $special_offer->inventory_type = $request->inventory_type;
        $special_offer->product_name = $request->product_name;
        $special_offer->client = implode(',', $request->client);
        $special_offer->selling_start_date = $request->selling_start_date;
        $special_offer->selling_end_date = $request->selling_end_date;
        $special_offer->travel_start_date = $request->travel_start_date;
        $special_offer->travel_end_date = $request->travel_end_date;
        $special_offer->original_price = $request->original_price;
        if($request->children_discount) {
            $special_offer->children_discount = 'Yes';
            $special_offer->children_discount_offer = $request->children_discounted_offer;
            $special_offer->children_discount_price = $request->children_discounted_price;
        }   else {
            $special_offer->children_discount = 'No';
        }
        $special_offer->discount_offer = $request->discounted_offer;
        $special_offer->discount_price= $request->discounted_price;
        $special_offer->offer_description = $request->offer_description;
        $special_offer->term_option = $request->term_option;
        $special_offer->additional_services = $request->additional_services;
        $special_offer->terms_n_conditions = $request->terms_n_conditions;
        $special_offer->cost_per_night = $request->cost_per_night;
        $special_offer->original_nights = $request->original_nights;
        $special_offer->free_nights = $request->free_nights;
        $special_offer->fee = $request->fee;

        $special_offer->save();
        Session::flash('message', 'SpecialOffer added successfully');
        return ['result' => 1,'msg'=>'SpecialOffer added successfully.'];
        //return redirect()->route('special-offer.list');

    }

    public function update(Request $request, SpecialOffer $special_offer)
    {
        $rules = [
            'offer_name' => 'required',
            'offer_type' => 'required',
            'inventory_type' => 'required',
            'product_name' => 'required',
            'client' => 'required',
            'selling_start_date' => 'required',
            'selling_end_date' => 'required',
            'travel_start_date' => 'required',
            'travel_end_date' => 'required'
        ];  

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return ['result' => 0,'errors' =>$validator->messages()];
        }

        $special_offer->offer_name = $request->offer_name;
        $special_offer->offer_type = $request->offer_type;
        $special_offer->inventory_type = $request->inventory_type;
        $special_offer->product_name = $request->product_name;
        $special_offer->client = implode(',', $request->client);
        $special_offer->selling_start_date = $request->selling_start_date;
        $special_offer->selling_end_date = $request->selling_end_date;
        $special_offer->travel_start_date = $request->travel_start_date;
        $special_offer->travel_end_date = $request->travel_end_date;
        $special_offer->original_price = $request->original_price;
        if($request->children_discount) {
            $special_offer->children_discount = 'Yes';
            $special_offer->children_discount_offer = $request->children_discounted_offer;
            $special_offer->children_discount_price = $request->children_discounted_price;
        }   else {
            $special_offer->children_discount = 'No';
        }
        $special_offer->discount_offer = $request->discounted_offer;
        $special_offer->discount_price= $request->discounted_price;
        $special_offer->offer_description = $request->offer_description;
        $special_offer->term_option = $request->term_option;
        $special_offer->additional_services = $request->additional_services;
        $special_offer->terms_n_conditions = $request->terms_n_conditions;
        $special_offer->cost_per_night = $request->cost_per_night;
        $special_offer->original_nights = $request->original_nights;
        $special_offer->free_nights = $request->free_nights;
        $special_offer->fee = $request->fee;

        $special_offer->save();
        Session::flash('message', 'SpecialOffer updated successfully');
        return ['result' => 1,'msg'=>'SpecialOffer updated successfully.'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SpecialOffer  $specialOffer
     * @return \Illuminate\Http\Response
     */
   
    public function getInventoryProducts(Request $request) {
        $inventory = $request->inventory;
        $products = '<option value="">--Select Product--</option>';
        $result = [];
        if($inventory === 'Accommodation') {
            $result = SpecialOffer::getAccommodations();
        }
        if($inventory === 'Activity') {
            $result = SpecialOffer::getActivities();
        }
        if($inventory === 'Tour') {
            $result = SpecialOffer::getTours();
        }
        if($inventory === 'Transport') {
            $result = SpecialOffer::getTransports();
        }
  
        foreach ($result as $row) {
            if(isset($row['name'])) {
                $products.='<option value="'.$row['id'].'">'.$row['name'].'</option>';
            } else {
                $products.='<option value="'.$row['id'].'">'.$row['from_city_name'].' - '.$row['to_city_name'].' - '.$row['supplier_name'].'</option>';
            }
        }

        return $products;
    }

    public function getPrice(Request $request) {
        $inventory = $request->inventory;
        $product_id = $request->product_id;

        if($inventory === 'Accommodation') {
            $result = SpecialOffer::getAccommodationPrice($product_id);
        }
        if($inventory === 'Activity') {
            $result = SpecialOffer::getActivityPrice($product_id);
        }
        if($inventory === 'Tour') {
            $result = SpecialOffer::getTourPrice($product_id);
        }
        if($inventory === 'Transport') {
            $result = SpecialOffer::getTransportPrice($product_id);
        }
        return $result;
    }

    public function changeStatus() {
        $offers = Input::get('offers');
        $sAction = Input::get('action');

        DB::enableQueryLog();

        switch ( $sAction ) {
                case 'Deleted':
                        if($offers){
                            $update_data = array('status'=>'Deleted');
                            SpecialOffer::whereIn('id', $offers)->update($update_data);
                        }
                        $msg = 'Offer Deleted Successfully.';
                        break;
                
                case 'Active':
                        if($offers){
                            $update_data = array('status'=>'Active');
                            SpecialOffer::whereIn('id', $offers)->update($update_data);
                        }
                        $msg = 'Offer Activated Successfully.';
                        break;
                case 'Pending':
                         if($offers){
                            $update_data = array('status'=>'Pending');
                            SpecialOffer::whereIn('id', $offers)->update($update_data);
                        }
                        $msg = 'Offer status chnaged to Pending.';
                        break;
                 case 'Expired':
                         if($offers){
                            $update_data = array('status'=>'Expired');
                            SpecialOffer::whereIn('id', $offers)->update($update_data);
                        }
                        $msg = 'Offer status chnaged to Expired';
                        break;
                default:
                        break;
        }
        Session::flash('message', $msg);
        return 'success';

    }
    
    //image upload on AWS test controller function
    public function imageUpload()
    {
    	return \View::make('WebView::special_offer.image_upload');
    }
    //test img upload<< priya
    public function imageUploadPost(Request $request)
    {
    	$this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $image = $request->file('image');
        $filePath = 'tours/' . $imageName;
        $t = \Storage::disk('s3')->put($filePath, file_get_contents($image), 'public');
        $imageName = \Storage::disk('s3')->url($filePath);


    	return back()
    		->with('success','Image Uploaded successfully.')
    		->with('path',$imageName);
    }

}
