<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItenaryLeg extends Model
{
    protected $fillable = [
                            'itenary_leg_id','itenary_order_id','from_city_name','to_city_id','to_city_name','from_city_id','country_code','from_date','to_date'
                        ];
    protected $table = 'itenarylegs';
    protected $primaryKey = 'itenary_leg_id';
}
