<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomainProduct extends Model
{
    protected $table = 'domain_products';
    protected $fillable = ['licensee_id','domain_id','product_id'];

    public function product(){
    	return $this->belongsTo('App\Product','product_id');
    }
}
