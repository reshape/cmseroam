<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'viator' => [
        'viator_url'        => 'http://prelive.viatorapi.viator.com/service',
        'api_key'           => '6050770146812181',  
    ],

    'gadventure' => [
        'gadventure_url'    => 'https://rest.gadventures.com/',
        'api_key'           => 'live_41385f08d2ea2c2f004babe73079ead1a8f93f81',  
    ],


    'mystifly' => [
        'account_number'  => 'MCN000201',
        'user_name'        => 'ATGXML',  
        'password'        => 'ATG2017_XML',  
        'target'          => 'Test',  
        'endpoint_url'    => 'http://onepointdemo.myfarebox.com/V2/OnePoint.svc?singleWsdl',  
        'PricingSourceType'    => 'All',  
        'IsRefundable'    =>  1,  
        'IsResidentFare'    => 0,  
        'RequestOptions'    => 'Fifty',  
        'NearByAirports'    => 0, 
        'DepartureTime'    => '00:00:00',  
        'AirTripType'    => 'OneWay',  
        'MaxStopsQuantity'    => 'All',  
        'VendorPreferenceCodes'    => NULL,  
    ],
];

