<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblprovidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tblproviders'))
        {
        Schema::create('tblproviders', function (Blueprint $table) {
            $table->increments('provider_id');
            $table->string('provider_name')->nullable();
            $table->tinyInteger('is_xml');
            $table->integer('currency_id')->nullable();
            $table->string('Logo',500)->nullable();
            $table->decimal('markup',16,4);
            $table->integer('Group_ID')->nullable();
            $table->tinyInteger('is_active')->nullable();
            $table->string('abbriviation')->nullable();
            $table->string('folder_path')->nullable();
            $table->string('Password')->nullable();
            $table->string('login_name')->nullable();
            $table->string('url')->nullable();
            $table->string('account_contacts')->nullable();
            $table->text('reservation_contacts')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_keyword')->nullable();
            $table->text('meta_desc')->nullable();
            $table->text('special_notes')->nullable();
            $table->longText('supplier_desc')->nullable();
            $table->longText('standard_remarks')->nullable();
            $table->string('sp_name')->nullable();
            $table->string('sp_title')->nullable();
            $table->string('sp_email')->nullable();
            $table->string('sp_phone')->nullable();
            $table->string('sr_name')->nullable();
            $table->string('sr_email')->nullable();
            $table->string('sr_landline')->nullable();
            $table->string('sr_freephone')->nullable();
            $table->string('sa_name')->nullable();
            $table->string('sa_title')->nullable();
            $table->string('sa_email')->nullable();
            $table->string('sa_phone')->nullable();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblproviders');
    }
}
