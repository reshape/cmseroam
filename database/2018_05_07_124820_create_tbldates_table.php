<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbldatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbldates', function (Blueprint $table) {
            $table->increments('Date_ID');
            $table->integer('tour_id');
            $table->dateTime('StartDate');
            $table->dateTime('EndDate');
            $table->decimal('AdultPrice',16,4)->nullable();
            $table->decimal('ChildPrice',16,4)->nullable();
            $table->integer('Availability')->nullable();
            $table->integer('adv_purchase')->nullable();
            $table->integer('season_id')->nullable();
            $table->decimal('AdultPriceSingle',16,4)->nullable();
            $table->decimal('ChildPriceSingle',16,4)->nullable();
            $table->decimal('AdultSupplement',16,4)->nullable();
            $table->decimal('ChildSupplement',16,4)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbldates');
    }
}
