<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassengerinformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passengerinformations', function (Blueprint $table) {
            $table->bigIncrements('passenger_information_id');
            $table->bigInteger('itenary_order_id')->nullable();
            $table->string('title')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('contact')->nullable();
            $table->string('gender')->nullable();
            $table->integer('country')->nullable();
            $table->date('dob')->nullable();
            $table->date('passport_expiry_date')->nullable();
            $table->string('passport_num')->nullable();
            $table->text('address_one')->nullable();
            $table->text('address_two')->nullable();
            $table->string('suburb')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('is_lead')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passengerinformations');
    }
}
