<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZviatorlocationsmappedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zviatorlocationsmapped'))
        {
        Schema::create('zviatorlocationsmapped', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->unsigned();
            $table->bigInteger('viator_destination_id');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('city_id')->references('id')->on('zcities');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zviatorlocationsmapped');
    }
}
