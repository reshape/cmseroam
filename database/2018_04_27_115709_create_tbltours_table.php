<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbltoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tbltours'))
        {
        Schema::create('tbltours', function (Blueprint $table) {
            $table->increments('tour_id');
            $table->string('code',50)->nullable();
            $table->string('tour_code',50)->nullable();
            $table->string('tour_title',200)->nullable();
            $table->string('tour_url',200)->nullable();
            $table->integer('discount')->nullable();
            $table->string('destination',100)->nullable();
            $table->decimal('saving_per_person',16,2)->nullable();
            $table->string('departure',100)->nullable();
            $table->integer('tour_type_logo_id')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->string('no_of_days_text',50)->nullable();
            $table->string('no_of_days',50)->nullable();
            $table->string('durationType',50)->nullable();
            $table->decimal('price',16,4)->nullable();
            $table->text('short_description')->nullable();
            $table->text('transport')->nullable();
            $table->text('long_description')->nullable();
            $table->string('grade',50)->nullable();
            $table->integer('region_id')->nullable();
            $table->integer('groupsize_min')->nullable();
            $table->integer('groupsize_max')->nullable();
            $table->text('grouptext')->nullable();
            $table->text('mapfile')->nullable();
            $table->text('food')->nullable();
            $table->text('accommodation')->nullable();
            $table->string('travel_guide')->nullable();
            $table->text('additional_info')->nullable();
            $table->integer('provider');
            $table->tinyInteger('is_active');
            $table->string('_year',50)->nullable();
            $table->string('url',50)->nullable();
            $table->string('tripStyle',50)->nullable();
            $table->string('brochureSupplier',50)->nullable();
            $table->text('tripCountries')->nullable();
            $table->text('tripActivities')->nullable();
            $table->string('tripRegion')->nullable();
            $table->string('serviceLevel')->nullable();
            $table->string('RBCode')->nullable();
            $table->dateTime('LastUpdate')->nullable();
            $table->tinyInteger('is_sync')->nullable();
            $table->tinyInteger('is_childAllowed');
            $table->string('children_age',100)->nullable();
            $table->dateTime('Date_LastUpdate')->nullable();
            $table->bigInteger('views')->nullable();
            $table->text('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->string('tour_currency',100)->nullable();
            $table->text('tour_PracticalDetail')->nullable();
            $table->tinyInteger('IsLive_PracticalDetail');
            $table->text('xml_PracticalDetail')->nullable();
            $table->text('xml_itinerary')->nullable();
            $table->tinyInteger('is_reviewed')->nullable();
            $table->tinyInteger('is_approve')->nullable();
            $table->text('sync_error')->nullable();
            $table->text('tour_remarks')->nullable();
            $table->tinyInteger('is_deleted')->nullable();
            $table->string('delete_by')->nullable();
            $table->integer('id_delete_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->integer('admin_id_updated')->nullable();
            $table->dateTime('date_admin_updated')->nullable();
            $table->integer('agent_id_updeted')->nullable();
            $table->dateTime('date_agent_updated')->nullable();
            $table->integer('provider_id_updated')->nullable();
            $table->dateTime('date_provider_updated')->nullable();
            $table->text('updated_msg')->nullable();
            $table->string('provider_tour_id')->nullable();
            $table->string('from_city_id')->nullable();
            $table->string('to_city_id')->nullable();
            $table->string('operator_id')->nullable();
            $table->string('supplier_id')->nullable();
            $table->string('de_countries')->nullable();
            $table->decimal('AdultPriceSingle',16,4)->nullable();
            $table->decimal('ChildPriceSingle',16,4)->nullable();
            $table->decimal('AdultSupplement',16,4)->nullable();
            $table->decimal('ChildPrice',16,4)->nullable();
            $table->decimal('retailcost',16,4)->nullable();
            $table->decimal('flightPrice',16,4)->nullable();
            $table->integer('flight_currency_id')->nullable();
            $table->text('flightDescription')->nullable();
            $table->text('flightReturn')->nullable();
            $table->text('flightDepart')->nullable();
            $table->string('countryData')->nullable();
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbltours');
    }
}
