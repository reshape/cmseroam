<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZaecountries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zaecountries'))
        {
        Schema::create('zaecountries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('CountryId')->nullable();
            $table->string('Code',255)->nullable();
            $table->string('name',255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zaecountries');
    }
}
