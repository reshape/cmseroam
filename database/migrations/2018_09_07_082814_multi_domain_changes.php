<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MultiDomainChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = explode(',', 'itenaryorders,saved_trips,special_offers,tblbookingrequests,tbltours,zactivity,zcities,zcountries,zhotel,zhotels,zregions,zroutes,zsuppliers,ztransports,ztransportsuppliers');
        foreach ($tables as $tableName) {
            if(!Schema::hasColumn($tableName, 'domain_id')) //check whether users table has email column
            {
            Schema::table($tableName, function (Blueprint $table) {
                $table->integer('domain_id')->default(45);
            });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = explode(',', 'itenaryorders,saved_trips,special_offers,tblbookingrequests,tbltours,zactivity,zcities,zcountries,zhotel,zhotels,zregions,zroutes,zsuppliers,ztransports,ztransportsuppliers');
        foreach ($tables as $tableName) {
            Schema::table($tableName, function (Blueprint $table) {
                $table->dropColumn('domain_id');
            });
        }
    }
}
