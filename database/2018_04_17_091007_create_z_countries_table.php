<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zcountries')){ 
            Schema::create('zcountries', function (Blueprint $table) {
                $table->increments('id');
                $table->string('code');
                $table->string('name');
                $table->string('continent_id')->nullable();
                $table->string('iso_numeric_code')->nullable();
                $table->string('iso_3_letter_code')->nullable();
                $table->tinyInteger('show_on_eroam')->nullable();
                $table->integer('region_id')->unsigned()->nullable();                           
                $table->timestamps();
                $table->softDeletes();

                $table->foreign('region_id')->references('id')->on('zregions');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zcountries');
    }
}
