<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $aData['leg'][0]['from_city_name'].'_HotelVoucher_'.$aData['invoiceNumber'].'.pdf';?></title>
    <style type="text/css">
      @page {
        margin: 15px;
      }
      body{
        font-family: Arial;
        color: #212121;
        font-size: 14px;
        margin: 0px;
      }
      p{
        margin: 5px 0;
      }
      ul li{ padding: 4px 0; }
      h2{margin: 5px 0;}
      table tr th{margin-bottom: 30px;}
    </style>
  </head>
  <body>
    <?php 
            $logo = getDomainLogo();
            if(!$logo) {
                    $logo = public_path('/images/logo-big.png');
            }
    ?>
    <table width="100%" cellpadding="4">
      <tbody>
        <tr>
          <td colspan="2">
            <table width="100%" style="background-color: #212121; color: #fff;">
              <tr>
                <td style="padding: 0 10px;">
                  <img src="{{ $logo }}" alt="eRoam" style="width: 100px;">
                </td>
                <td style="text-align: right; padding: 0px 15px; vertical-align: middle;">
                  <h2 style="margin-top: 4px;"><img src="{{ public_path('/images/ic_local_hotel.svg') }}" alt="" style="position: relative; top: 3px; margin-top: 4px;"/> Hotel Voucher</h2>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%" style="border: solid 3px #fafafa;text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
              <thead>
                <tr style="background-color: #fafafa;">
                  <th colspan="3">
                    <h2 style="margin: 8px 8px 8px 2px;">Booking Details</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <p><strong>Guest Name:</strong> {{$aRequestData['billing_first_name']}}</p>
                    <p><strong>Email:</strong> {{$aRequestData['billing_email']}}</p>
                  </td>
                  <td>
                      <p><strong>Booking Id:</strong> {{$aData['invoiceNumber']}}</p>
                      <p><strong>Booking Date:</strong> {{ date("F j, Y, g:i a") }}</p>
                  </td>
                </tr>
                <tr>
                  <td>
                      <h3 style="margin: 0;">{{$aData['leg'][0]['from_city_name'].', '.$aData['leg'][0]['country_code'].' - '.$aHotel['leg_name']}}</h3>
                  </td>
                  <td>
                      <p><strong>Hotel Booking ID:</strong>{{ $aData['invoiceNumber'] }}</p>
                  </td>
                </tr>
                <tr>
                    <td>
                      {{$aHotel['address']}}<br>{{$aData['leg'][0]['from_city_name'].', '.$aData['leg'][0]['country_code']}}
                    </td>
                    <td>
                      <strong>Check In</strong><br>{{$aHotel['checkin']}}
                  </td>
                  <td>
                      <strong>Check Out</strong><br>{{$aHotel['checkout']}}
                  </td>
                </tr>
                <tr>
                  <td colspan="3">
                      
                    <h3 style="margin: 5px 0">Total payment to be made - {{ $aHotel['currency'] }} {{ $aHotel['hotel_expedia_total'] }}</h3>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%" border="1" style="border: solid 3px #fafafa; text-align: center; border-collapse: collapse; margin-top: 5px;" cellspacing="4" cellpadding="10">
              <tr>
                <td style="border-color:#fafafa;">
                    <h3 style="margin: 0;">Check-in Date<!--  &amp; Time --></h3>
                    <p>{{ $aHotel['checkin'] }}</p>
                    <!-- <p>12 PM Onwards</p> -->
                </td>
                <td style="border-color:#fafafa;">
                    <h3 style="margin: 0;">Nights</h3>
                    <p>{{ $aHotel['nights'] }} Nights</p>
                </td>
                <td style="border-color:#fafafa;">
                    <h3 style="margin: 0;">Check-out Date<!--  &amp; Time --></h3>
                    <p>{{ $aHotel['checkout'] }}</p> 
                    <!-- <p>Till 11 AM</p> -->
                </td>
                <td style="border-color:#fafafa;">
                    <h3 style="margin: 0;">Guests</h3>
                    <p>{{ $aData['adult'] }} Adult {{!empty($aData['child']) ? ', '.$aData['child'].' Child':''}}</p>
                </td>
                <td style="border-color:#fafafa;">
                    <h3 style="margin: 0;">{{ $aHotel['hotel_total_room'] }} Rooms</h3>
                    
                </td>
              </tr>
            </table>
          </td>
        </tr>


        <tr>
            <td colspan="2">
              <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
                <thead>
                  <tr style="background-color: #fafafa;">
                    <th colspan="3">
                      <h2 style="margin: 8px 8px 8px 2px;">Payment Summary</h2>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <strong>Room Tariff</strong>
                    </td>
                    <td>
                      
                        {{ $aHotel['currency'] }} {{ $aHotel['hotel_price_per_night'] }} x {{$aHotel['nights']}} Nights x {{$aHotel['hotel_total_room']}} Rooms 
                    </td>
                    <td style="text-align: right; padding-right: 20px;">
                        {{$aHotel['currency'].' '.$aHotel['hotel_expedia_total']}}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Other Charges</strong><br>
                      Tax Recovery & Service Fee<br>
                      Hotel Fees
                    </td>
                    <td><br>
                      {{ $aHotel['currency'].' '.number_format($aHotel['hotel_tax'],2) }}<br>
                    </td>
                    <td style="text-align: right; padding-right: 20px;">	
                       <strong>{{$aHotel['currency'].' '.number_format(($aHotel['hotel_tax']+$aHotel['hotel_eroam_subtotal']),2)}}</strong><br>
                      
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Grand Total</strong>
                    </td>
                    <td></td>
                    <td style="text-align: right; padding-right: 20px;">
                      <strong>{{$aData['currency']}} {{ $aHotel['hotel_eroam_subtotal'] }}</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Advance Paid</strong>
                    </td>
                    <td>
                      Paid through Credit Card ({{ $aHotel['currency'].' '.$aHotel['hotel_eroam_subtotal'] }})
                    </td>
                    <td style="text-align: right; padding-right: 20px;">
                      {{$aHotel['currency']}} {{ $aHotel['hotel_eroam_subtotal'] }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Total Payable Amount </strong><br>All Inclusive
                    </td>
                    <td>
                      <strong>Pay at Check-in</strong>
                    </td>
                    <td style="text-align: right; padding-right: 20px;">
                      <strong>{{$aHotel['currency'].' '.number_format($aHotel['hotel_eroam_subtotal'],2)}}</strong>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
              <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
                <thead>
                  <tr style="background-color: #fafafa;">
                    <th>
                      <h2 style="margin: 8px 8px 8px 2px;">Cancellation &amp; Amendment Policy</h2>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <ul>
                        @isset($aHotel['cancellationPolicy'])
                        <li>{!!html_entity_decode($aHotel['cancellationPolicy'])!!}</li>
                        @endisset
                       
                      </ul>
                    </td>
                  </tr>
                  
                </tbody>
              </table>
            </td>
        </tr>

        @isset($aHotel['checkin_instruction'])
        <tr>
          <td colspan="2">
            <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
              <thead>
                <tr style="background-color: #fafafa;">
                  <th>
                    <h2 style="margin: 8px 8px 8px 2px;">@lang('home.hotel_checkin_instruction_text')</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <p>
                      @php
                        $aHotel['checkin_instruction'] = strstr($aHotel['checkin_instruction'], '<p><b>Fees</b>');
                        $aHotel['checkin_instruction'] = str_replace('<p><b>Fees</b>','<p><strong>Fees / Optional Extras</strong>',$aHotel['checkin_instruction']);
                      @endphp
                      {!!html_entity_decode($aHotel['checkin_instruction'])!!}
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        @endisset

        @isset($aHotel['amenities_description'])
        <tr>
          <td colspan="2">
            <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
              <thead>
                <tr style="background-color: #fafafa;">
                  <th>
                    <h2 style="margin: 8px 8px 8px 2px;">Amenities Instructions</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <p>{!!html_entity_decode($aHotel['amenities_description'])!!}</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        @endisset
        <tr>
          <td>
            <p style="margin-top: 40px; font-size: 16px;">We wish you a hassle-free stay.<br>
              Waiting to host you</p>
              
            <p style="margin-top: 20px; font-size: 16px;">See you soon,<br>
              <strong>Team eRoam</strong></p>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>